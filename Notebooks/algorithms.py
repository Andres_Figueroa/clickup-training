import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np
from scipy import stats

def create_full_percentage_pivot_table(df,win_percentage_colname,turnout_percentage_colname,keep_cols=None):
    MAX_PERCENTAGE_VALUE =100
    SIZE_COLNAME = "size"
    percentages=np.arange(0,101)
    
    # Round percentage columns 
    df.loc[:,win_percentage_colname]=df.loc[:,win_percentage_colname].astype(int)
    df.loc[:,turnout_percentage_colname]=df.loc[:,turnout_percentage_colname].astype(int)
    
    index = pd.MultiIndex.from_product([percentages, percentages], names = [turnout_percentage_colname, win_percentage_colname])

    all_percentage_comb_df=pd.DataFrame(index = index).reset_index()
    if not keep_cols:
        units_per_turnout_win_df= df.groupby([turnout_percentage_colname,win_percentage_colname]).size().reset_index(name=SIZE_COLNAME)
    else:
        units_per_turnout_win_df = df.groupby([turnout_percentage_colname,win_percentage_colname]+keep_cols).size().reset_index(name=SIZE_COLNAME)
    units_per_turnout_win_df = all_percentage_comb_df.merge(units_per_turnout_win_df,on= [turnout_percentage_colname, win_percentage_colname],how="left")
    units_per_turnout_win_df.loc[:,units_per_turnout_win_df.dtypes=="object"]=units_per_turnout_win_df.loc[:,units_per_turnout_win_df.dtypes=="object"].fillna("null")
    units_per_turnout_win_df.loc[:,units_per_turnout_win_df.dtypes=="float"]=units_per_turnout_win_df.loc[:,units_per_turnout_win_df.dtypes=="float"].fillna(0)
    units_per_turnout_win_df.loc[:,units_per_turnout_win_df.dtypes=="int"] =units_per_turnout_win_df.loc[:,units_per_turnout_win_df.dtypes=="int"].fillna(0)
    
    
    fgp_df =  pd.pivot_table(units_per_turnout_win_df,index=win_percentage_colname,columns=turnout_percentage_colname,values=SIZE_COLNAME,aggfunc='sum').sort_values(by=win_percentage_colname,ascending=False).fillna(0)

    return fgp_df, units_per_turnout_win_df

def plot_fingerprint(df,win_percentage_colname,turnout_percentage_colname,unit="Unit",figsize=(15,15),cmap="gnuplot",show=True,title="",keep_cols=None,vmin=None,center=None,vmax=None):
    full_fgp_df,units_per_turnout_win_df = create_full_percentage_pivot_table(df,win_percentage_colname,turnout_percentage_colname,keep_cols)
       
    fig = plt.figure(figsize=figsize)
    ax= plt.gca()
    sns.heatmap( full_fgp_df,ax=ax,cmap=cmap,vmin=vmin,center=center,vmax=vmax)
    plt.title(title+". Unidad de agregación: "+unit,weight='bold').set_fontsize('24')
    plt.xlabel("Porcentaje de participación",weight='bold').set_fontsize('20')
    plt.ylabel("Porcentaje de votos para el partido", weight='bold').set_fontsize('20')
    if not show:
        plt.close()
    return full_fgp_df,units_per_turnout_win_df,fig

def gen_s_curve_df(df,agg_unit_id_col, parties_col,vote_qty_col,registered_col,turnout_per_col="turnout_percentage",vote_qty_cumulative_col="vote_qty_cumulative",current_party_per_col="current_party_percentage",official_percentage=False):
    VALID_VOTES_COLNAME="valid_votes"
    valid_votes_by_agg_unit_df = df.groupby(agg_unit_id_col,as_index=False).agg({vote_qty_col:sum}).rename({vote_qty_col:VALID_VOTES_COLNAME},axis=1)
    add_reg_df = df.merge(valid_votes_by_agg_unit_df,on=agg_unit_id_col,how="inner")
    
    add_reg_df.loc[:,turnout_per_col] = ((add_reg_df.loc[:,VALID_VOTES_COLNAME]*100)/add_reg_df.loc[:,registered_col]).round()
    if not official_percentage:
        total_registered = df.drop_duplicates(subset=agg_unit_id_col).loc[:,registered_col].sum()
    else:
        total_registered = df.loc[:,vote_qty_col].sum()
    turnout_per_parties_grouped_df = add_reg_df.groupby([turnout_per_col,parties_col],as_index=False).agg({vote_qty_col:sum})
    party_df_groups = turnout_per_parties_grouped_df.groupby(parties_col)
    party_groups_df_list = []
    for (party,df) in party_df_groups:
        df= df.sort_values(by=turnout_per_col)
        df.loc[:,vote_qty_cumulative_col]= df.loc[:,vote_qty_col].cumsum()
        df.loc[:,current_party_per_col] = df.loc[:,vote_qty_cumulative_col] * 100/total_registered
        party_groups_df_list.append(df)
    return pd.concat(party_groups_df_list)

def plot_s_curves(df,agg_unit_id_col,parties_col,vote_qty_col,registered_col,turnout_per_col="turnout_percentage",vote_qty_cumulative_col="vote_qty_cumulative",current_party_per_col="current_party_percentage",figsize=(15,15),xlabel="turnout percentage",ylabel="parties' vote percentage",title="Parties' S curve",official_percentage=False):
    s_curve_df = gen_s_curve_df(df,agg_unit_id_col,parties_col,vote_qty_col,registered_col,turnout_per_col=turnout_per_col,vote_qty_cumulative_col=vote_qty_cumulative_col,current_party_per_col=current_party_per_col,official_percentage=official_percentage)
    parties_groups = s_curve_df.groupby(parties_col)
    fig=plt.figure(figsize=figsize)
    ax=plt.gca()
    legend=[]
    for (party,party_s_curve_df) in parties_groups:
        party_s_curve_df.plot(x=turnout_per_col,y=current_party_per_col,ax=ax,grid=True,legend=True,xlabel=xlabel,ylabel=ylabel,title=title)
        legend.append(party)
    ax.legend(legend)
    return fig, s_curve_df
    
    
    
    
    