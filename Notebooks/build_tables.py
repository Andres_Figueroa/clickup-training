import pandas as pd
import numpy as np

def create_new_objects_and_pk_for_table(
    df,
    comp_df,
    pk_col,
    common_col,
    relevant_cols,
    merge_df=None,
    left_on=None,
    right_on=None,
    how="inner",
    return_full=False,
):
    if type(common_col) is str:
        common_col=[common_col]
    unique_elements_df = df[common_col].drop_duplicates()
    unique_elements_comp_df =comp_df[common_col].drop_duplicates()
    remaining_values = pd.concat([unique_elements_df,unique_elements_comp_df,unique_elements_comp_df]).drop_duplicates(keep=False).reset_index(drop=True)
    print("Remaining values shape: {}".format(remaining_values.shape))
    if not remaining_values.empty:
        if comp_df.empty:
            last_pk=0
        else:
            last_pk = comp_df[pk_col].max()
        new_pks = np.arange(last_pk + 1, last_pk + len(remaining_values) + 1)
        rem_val_pks_df = pd.concat([pd.DataFrame({pk_col: new_pks}), remaining_values.sort_values(by=common_col)],axis=1)
        add_df = rem_val_pks_df.merge(df, on=common_col, how="inner")
        if merge_df is not None:
            if left_on and right_on:
                add_df = add_df.merge(
                    merge_df, left_on=left_on, right_on=right_on, how=how
                )
                if return_full:
                    return pd.concat(
                        [comp_df[relevant_cols], add_df[relevant_cols]]
                    ).drop_duplicates()
                else:
                    return add_df[relevant_cols].drop_duplicates()
            else:
                raise ValueError(
                    "left_on and right_on arguments should not be None when merge_df exists"
                )
        else:
            if return_full:
                return pd.concat(
                    [comp_df[relevant_cols], add_df[relevant_cols]]
                ).drop_duplicates()
            else:
                return add_df[relevant_cols].drop_duplicates()
                
    else:
        return pd.DataFrame([],columns=relevant_cols)
        
def create_new_objects_for_table(
    df,
    comp_df,
    pk_col,
    relevant_cols,
    merge_df=None,
    left_on=None,
    right_on=None,
    how="inner",
    return_full = False,
):
    unique_elements_set = set(df[pk_col])
    unique_elements_comp_set(comp_df[pk_col])
    remaining_pk = list(unique_elements_set.difference(unique_elements_comp_set))
    if remaining_pk:
        add_df = df[df[pk_col].isin(remaining_pk)]
        if merge_df is None:
            if return_full:
                return pd.concat([comp_df[relevant_cols], add_df[relevant_cols]]).drop_duplicates()
            else:
                return add_df[relevant_cols].drop_duplicates()
        else:
            if left_on and right_on:
                add_df = add_df.merge(
                    merge_df, left_on=left_on, right_on=right_on, how=how
                )
                if return_full:
                    return pd.concat([comp_df[relevant_cols], add_df[relevant_cols]]).drop_duplicates() 
                else:
                    return add_df[relevant_cols].drop_duplicates()
            else:
                raise ValueError(
                    "left_on and right_on arguments should not be None when merge_df exists"
                )
    else:
        return comp_df[relevant_cols].drop_duplicates()