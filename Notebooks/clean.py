#install first pandas and unidecode with pip
from unidecode import unidecode
import pandas as pd
import os
import re
from datetime import datetime
import sys

columns_to_clean = ['pais','departamento','provincia', 'municipio', 'circunscripcion', 'localidad', 'recinto', 'eleccion']
#columns_to_clean = ['partido','nombre','izquierda/derecha', 'ref_parties']
#put the script in the same folder as the cleaning target, change the relative path as the cleaning script
relative_path = 'TREP_2019'
#relative_path = 'TREP_2019'

def read_file(path):
    
    #return pd.read_csv(relative_path+'/'+path)
    return pd.read_csv(os.getcwd()+ "/" +relative_path+"/"+path)

def clean_column_names(data):
    data.columns = data.columns.str.lower()
    data.columns = data.columns.str.replace("ñ","}")
    data.columns = [unidecode(column) for column in data.columns]
    data.columns = data.columns.str.replace("}","ñ")
    return data.columns
    
def clean_String(x):
    x = str.lower(str(x))
    x = x.strip()
    x = x.replace('"',"'")
    x = x.replace('º','')
    x = x.replace('°','')
    x = x.replace('ñ','{')
    x = unidecode(x)
    x = x.replace('{','ñ')
    return x

def replace_data(data, new_data):
    for column in columns_to_clean:
        data[column] = new_data[column]
    return data

def save_as_csv(data, file):

    #file = file.replace('xlsx','csv')
    data.to_csv(os.getcwd() + "/" + relative_path + "/" + file, index=None)

def cleaning(data):
    
    data.columns = clean_column_names(data)
    data_strings = data[[columns_to_clean[0], columns_to_clean[1], columns_to_clean[2], columns_to_clean[3],\
                         columns_to_clean[4],columns_to_clean[5], columns_to_clean[6], columns_to_clean[7]]]
    #data_strings = data[[columns_to_clean[0], columns_to_clean[1], columns_to_clean[2], columns_to_clean[3]]]
    data_strings = data_strings.applymap(lambda x: clean_String(x))
    data = replace_data(data, data_strings)    
    return data

def list_data():
    files = os.listdir(os.getcwd() + '/' + relative_path)
    #files.remove(".ipynb_checkpoints")
    return files
    
def cleanandjoin():
    startTime = datetime.now()    
    files = list_data()
    for file in files:
        data =  read_file(file)
        data = cleaning(data)
        save_as_csv(data, file)
    time_to_execute=datetime.now() - startTime 
    print(time_to_execute)
    
cleanandjoin()
