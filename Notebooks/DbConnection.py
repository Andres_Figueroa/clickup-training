from sqlalchemy import MetaData, create_engine
from sqlalchemy.orm import sessionmaker
import pandas as pd
import psycopg2
conn_str ="postgresql://electionData:electionDataPassword@34.122.17.115:5000/electionData2020"
engine = create_engine(conn_str)

def get_elecciones():
    table = pd.read_sql_table(
        'elecciones',
        con=engine
    )
    return table

def get_municipio():
    table = pd.read_sql_table(
        'municipio',
        con=engine
    )
    return table

def get_partido():
    table = pd.read_sql_table(
        'partido',
        con=engine
    )
    return table

def get_municipio_variante_en_tiempo():
    table = pd.read_sql_table(
        'municipio_variante_en_tiempo',
        con=engine
    )
    return table

def get_departamento_variante_en_tiempo():
    table = pd.read_sql_table(
        'departamento_variante_en_tiempo',
        con=engine
    )
    return table

def get_departamento():
    table = pd.read_sql_table(
        'departamento',
        con=engine
    )
    return table

def get_votos():
    table = pd.read_sql_table(
        'votos',
        con=engine
    )
    return table

def get_recinto():
    table = pd.read_sql_table(
        'recinto',
        con=engine
    )
    return table

def get_mesa():
    table = pd.read_sql_table(
        'mesa',
        con=engine
    )
    return table