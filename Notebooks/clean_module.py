#install first pandas and unidecode with pip
from unidecode import unidecode
import pandas as pd
import os
import re
from datetime import datetime
import sys

# columns_to_clean = ['pais','departamento','provincia', 'municipio', 'circunscripcion', 'localidad', 'recinto', 'eleccion']
#columns_to_clean = ['partido','nombre','izquierda/derecha', 'ref_parties']
#put the script in the same folder as the cleaning target, change the relative path as the cleaning script
# relative_path = 'TREP_2019'
#relative_path = 'TREP_2019'

def read_file_from_rel_path(path,relative_path,filetype="csv",encoding="utf-8-sig"):
    if filetype == "csv":
        return pd.read_csv(relative_path+"/"+path,encoding=encoding)
    elif filetype=="xls" or filetype == "xlsx":
        return pd.read_excel(relative_path+"/"+path)
    else:
        raise ValueError("filetype not handled")
    


def read_file(path,relative_path,filetype="csv",encoding="utf-8-sig"):
    if filetype == "csv":
        return pd.read_csv(os.getcwd()+ "/" +relative_path+"/"+path,encoding=encoding)
    elif filetype=="xls" or filetype == "xlsx":
        return pd.read_excel(os.getcwd()+ "/" +relative_path+"/"+path,encoding=encoding)
    else:
        raise ValueError("filetype not handled")
    

def clean_column_names(data):
    data.columns = data.columns.str.lower()
    data.columns = data.columns.str.replace("ñ","}")
    data.columns = [unidecode(column) for column in data.columns]
    data.columns = data.columns.str.replace("}","ñ")
    data.columns = data.columns.str.replace("mas-ipsp","mas")
    data.columns = data.columns.str.replace("mas - ipsp","mas")
    return data.columns
    
def clean_String(x):
    x = str.lower(str(x))
    x = x.strip()
    x = x.replace('"',"'")
    x = x.replace('º','')
    x = x.replace('°','')
    x = x.replace('ñ','{')
    x = unidecode(x)
    x = x.replace('{','ñ')
    return x

def replace_data(data, new_data,columns_to_clean):
    for column in columns_to_clean:
        data[column] = new_data[column]
    return data

def save_as_csv(data, relative_path,file):

    #file = file.replace('xlsx','csv')
    data.to_csv(os.getcwd() + "/" + relative_path + "/" + file, index=None)

def cleaning(data):
    
    data.columns = clean_column_names(data)
    dtypes = dict(data.dtypes)
    columns_to_clean = list(data.columns)
    data_strings = data[columns_to_clean]
    data_strings = data_strings.applymap(lambda x: clean_String(x))
    data = replace_data(data, data_strings,columns_to_clean)    
    for (dtype_key, dtype_value) in dtypes.items():
        data.loc[:, dtype_key] = data.loc[:, dtype_key].astype(dtype_value)
    return data

def list_data(relative_path):
    files = os.listdir(os.getcwd() + '/' + relative_path)
    #files.remove(".ipynb_checkpoints")
    return files
    
def cleanandjoin(relative_path):
    startTime = datetime.now()    
    files = list_data(relative_path)
    for file in files:
        data =  read_file(file)
        data = cleaning(data)
        save_as_csv(data, file)
    time_to_execute=datetime.now() - startTime 
    print(time_to_execute)
    
# def cartesian_product(a,b,a_colname,b_colname):
def cartesian_product(a,b):
    index = pd.MultiIndex.from_product([a, b])
    return pd.DataFrame(index = index).reset_index()

def replace_string_with_closest(string, options, d):
    calculate_distance = lambda x: levenshtein_distance(string, x)
    dists = options.apply(calculate_distance)
    if options[dists <= d].empty:
        return string
    else:
        return options[dists == dists.min()].iloc[0]