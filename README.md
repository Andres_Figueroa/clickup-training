# Election_data
- The project consists of a system with 4 stages:
    1. get data from a wep page publishing the results of elections
    2. clean the data 
    3. transform the data
    4. upload the processed data to a database
    5. create visualizations using the processed data
    6. Implement analyses to detect anomalous events
