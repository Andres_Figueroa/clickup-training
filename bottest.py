from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import requests
import time

from datetime import datetime




options = Options()
options.headless = True
options.add_argument("--window-size=1920,1200")
options.add_argument("--no-sandbox")
options.add_argument('--disable-dev-shm-usage')
options.add_argument("--disable-notifications")
options.add_argument("--headless")
options.add_argument('--verbose')
options.add_experimental_option("prefs", {
        "download.default_directory": "./DOWNLOADED_FILES",
        "download.prompt_for_download": False,
        "download.directory_upgrade": True,
        "safebrowsing_for_trusted_sources_enabled": False,
        "safebrowsing.enabled": False
})
options.add_argument('--disable-gpu')
options.add_argument('--disable-software-rasterizer')



DRIVER_PATH = '/usr/bin/chromedriver'

#enable_download_headless(driver, download_dir)
driver = webdriver.Chrome(options=options, executable_path=DRIVER_PATH)




pageURL="http://localhost:4999/"
driver.get(pageURL)
print("Scraping: " + driver.current_url)

time.sleep(300)
driver.quit()
print("Bot Called")


