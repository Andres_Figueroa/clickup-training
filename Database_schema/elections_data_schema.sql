--
-- PostgreSQL database dump
--

-- Dumped from database version 12.3
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: elections_db_schema; Type: SCHEMA; Schema: -; Owner: adrianvillarroel
--

CREATE SCHEMA elections_db_schema;


ALTER SCHEMA elections_db_schema OWNER TO adrianvillarroel;

--
-- PostgreSQL database dump complete
--

