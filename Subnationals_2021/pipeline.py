# %%
import pandas as pd
import numpy as np
import schedule
import openpyxl
import janitor
import os
import time

from algorithms.utils import *
from pipeline_pkg.upload import append_to_db_table
from pipeline_pkg.cleaning import clean_standard_form_df
from pipeline_pkg.comparison_with_existing_records import compare_with_existing_records
from pipeline_pkg.standard_form_transformer import check_and_transform_to_std_format
from pipeline_pkg.departamentos_var_t import fill_departamentos_var_t
from pipeline_pkg.municipios_var_t import fill_municipios_var_t
from pipeline_pkg.departamentos import fill_departamentos
from pipeline_pkg.municipios import fill_municipios
from pipeline_pkg.elections import fill_elecciones
from pipeline_pkg.partido import fill_partido
from pipeline_pkg.recinto import fill_recinto
from pipeline_pkg.mesa import fill_mesa
from pipeline_pkg.voto import fill_voto
from datetime import datetime
from functools import reduce
from sqlalchemy.exc import DBAPIError
from datetime import datetime, timedelta


# %%
def pipeline():
    year = 2021
    path_to_files = "../../DOWNLOADED_FILES/"
    path_to_processed_files = "../../DOWNLOADED_FILES/PROCESSED_FILES/"

    print(
        "-------------------------Transform to Standar Format-------------------------"
    )

    std_df = check_and_transform_to_std_format(
        folderpath=path_to_files,
        year=2021,
        election_code="ESN2021",
        check_newest_files=True,
    )

    print(
        "-------------------------Clean the standar format dataframe-------------------------"
    )
    cleaned_df = [clean_standard_form_df(df) for df in std_df.values()]

    print(
        "-------------------------Fill the elecciones table-------------------------"
    )
    fill_elecciones(year=year, con=con, update_to_db=True)

    print(
        "-------------------------Fill partido table-------------------------"
    )
    for df in cleaned_df:
        fill_partido(df, "subnational2015", "partido", con, year)

    print(
        "-------------------------Fill departamento table-------------------------"
    )
    for df in cleaned_df:
        fill_departamentos(df, "departamento", con)

    print(
        "-------------------------Fill municipio table-------------------------"
    )
    for df in cleaned_df:
        fill_municipios(df, "municipio", con)

    print(
        "-------------------------Fill departamento_variante_en_tiempo table-------------------------"
    )
    fill_departamentos_var_t(con, year, "departamento_variante_en_tiempo")

    print(
        "-------------------------Fill municipio_variante_en_tiempo table-------------------------"
    )
    for df in cleaned_df:
        fill_municipios_var_t(year, df, con, "municipio_variante_en_tiempo")

    print(
        "-------------------------Fill recinto table-------------------------"
    )
    for df in cleaned_df:
        fill_recinto(df, con, year)

    print("-------------------------Fill mesa table-------------------------")
    for df in cleaned_df:
        fill_mesa(df, con, year)

    print("-------------------------Fill voto table-------------------------")
    for df in cleaned_df:
        fill_voto(df, con, year)

    print(
        "-------------------------Move Processed Files-------------------------"
    )
    for name in std_df.keys():
        print(
            "Moving the file: \n\t{0} \n\tfrom {1} \n\tto {2}".format(
                name, path_to_files, path_to_processed_files
            )
        )
        os.replace(
            os.path.join(path_to_files, name),
            os.path.join(path_to_processed_files, name),
        )


# %%
if __name__ == "__main__":
    schedule.every(30).minutes.do(pipeline)
    while True:
        schedule.run_pending()
        time.sleep(1)


# %%
