--Agrupacion por fecha_hora_actualizacion
select mesa_pk,fecha_hora_actualizacion,partido_pk,tipo_de_voto,cantidad, count(*) as nrocasos_repetidos from public.votos
group by mesa_pk,fecha_hora_actualizacion,partido_pk,tipo_de_voto,cantidad
order by nrocasos_repetidos desc limit 20;

--1er caso: Hora: "2020-10-22 10:48:00"  2 casos repetidos  voto_pk: 13297109,12899918
select * from public.votos where mesa_pk=1001 and partido_pk=1 and tipo_de_voto='presidencial' and cantidad=2 and voto_pk in (13297109,12899918) 
order by fecha_hora_actualizacion;
--2� caso: "2020-10-20 02:18:35"    2 casos repetidos(735283,1390537)
select * from public.votos where mesa_pk=1001 and partido_pk=16 and tipo_de_voto='presidencial' and cantidad=34 and voto_pk in (735283,1390537)
order by fecha_hora_actualizacion;

--Agrupacion por fecha_hora_descarga
select mesa_pk,fecha_hora_descarga,partido_pk,tipo_de_voto,cantidad, count(*) as nrocasos_repetidos from public.votos
group by mesa_pk,fecha_hora_descarga,partido_pk,tipo_de_voto,cantidad
order by nrocasos_repetidos desc limit 20;

-- Nro de actualizaciones : total 23 ----agrupacion por fecha_hora_actualizacion
select fecha_hora_actualizacion, count(*) as nro_registros
from public.votos
group by fecha_hora_actualizacion
order by fecha_hora_actualizacion

-- Nro de descargas : total 25 ----agrupacion por fecha_hora_descarga
select fecha_hora_descarga, count(*) as nro_registros
from public.votos
group by fecha_hora_descarga
order by fecha_hora_descarga


-- Nro de descargas : total 25 ----agrupacion por fecha_hora_descarga
select fecha_hora_descarga, count(*) as nro_registros
from public.votos
group by fecha_hora_descarga
order by fecha_hora_descarga
