# %%
from Pipeline.utils import delete_table_data, con

# %%
tables_to_delete = ["votos","mesa","recinto","municipio_variante_en_tiempo","departamento_variante_en_tiempo","municipio","departamento","partido","elecciones"]

# %%
process_terminated = 0
while(process_terminated == 0):
    x = input(
        f"Are you really sure to delete data from these tables {tables_to_delete} (y/n): {tables_to_delete}")
    if x == 'y':
        for table in tables_to_delete:
            delete_table_data(table, con)
            print(f"Table: {table}, status: deleted")
        process_terminated = 1
    elif x == 'n':
        process_terminated = 1
    else:
        print("Only options 'y' or 'n' are allowed")


# %%
