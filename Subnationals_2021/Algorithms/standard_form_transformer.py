# %%
from datetime import datetime
import pandas as pd
import janitor
import openpyxl
import os
import time
from functools import reduce
from datetime import datetime, timedelta

# ---------------------------------------------------------------------------- #
#                              General guidelines
# - Add as many functions as necessary
# - Change the function definition if convenient and necessary (also the comments)
# - Try to do the code as clean as posible
# - Dataframe variables end with "_df" (E.g. votes_df)
# - Comment the code in the way that is shown here
# - Follow the style shown in the template
# - Test the main function with a small dataset
# (E.g. variables in snake case (E.g. votes_per_day), clases CamelCase if any)
# ---------------------------------------------------------------------------- #
# %%
EXCEL_ENGINE = "openpyxl"


def get_datetime_values(date_str, time_str):
    file_year = date_str[0:4]
    file_month = date_str[4:6]
    file_day = date_str[6:8]
    file_hour = time_str[0:2]
    file_min = time_str[2:4]
    file_sec = time_str[4:6]

    return (
        int(file_year),
        int(file_month),
        int(file_day),
        int(file_hour),
        int(file_min),
        int(file_sec),
    )


def check_and_transform_to_std_format(
    folderpath,
    year,
    election_code,
    departamentos_code=["TJ", "SC", "PO", "PA", "OR", "LP", "CB", "CH", "BE"],
    check_newest_files=False,
):
    """Check if there is any file at filepath, put them in a queue an transform them in a standard format

    In the caller of this function the original file should be moved to a folder  "Processed".
    The tranformed file should be put in a folder named "Standardized",
    In the caller this function should run periodically, always checking if there are several files to process. If there
    are several, the files should be put in queue.

    Args:
        folderpath(str): path to the folder containing the files saved by the
    Returns:
        dataframes in the standard format to be pushed in the pipeline

    """
    # election_code = 'ESN2021'

    file_names = os.listdir(folderpath)
    # print(file_names)
    newest_files_names = []
    if check_newest_files:
        for dpto in departamentos_code:
            tmp = 0
            newest_file_datetime = datetime(1, 1, 1, 0, 0, 0)
            newest_file_name = ""
            for csv in file_names:
                if csv.startswith(
                    "{0}_{1}".format(election_code, dpto)
                ) and csv.endswith(".xlsx"):
                    tmp += 1
                    splited = csv.split(".")
                    file_name_splited = splited[0].split("_")

                    tmp_datetime = get_datetime_values(
                        file_name_splited[2], file_name_splited[3]
                    )

                    file_datetime_var = datetime(
                        tmp_datetime[0],
                        tmp_datetime[1],
                        tmp_datetime[2],
                        tmp_datetime[3],
                        tmp_datetime[4],
                        tmp_datetime[5],
                    )

                    if file_datetime_var > newest_file_datetime:
                        newest_file_name = csv

            if newest_file_name != "":
                newest_files_names.append(newest_file_name)
    else:
        newest_files_names = file_names

    # Read the folder
    files_path_list = [
        os.path.join(folderpath, filename) for filename in newest_files_names
    ]

    # transform the list of files
    # return files_path_list
    std_dfs = [
        transform_to_standard_format(file_path, year)
        for file_path in files_path_list
    ]

    return dict(zip(newest_files_names, std_dfs))


def lower_case_colnames(df):
    df.columns = [col.lower() for col in df.columns]


def transform_to_standard_format(filepath, year):
    """Transforms a raw data file to standard format

    This function should save the raw file in a folder named "raw_files", the transformed dataframe into a "standard_format_files"

    Args:
        filepath(str): path to the file to be transformed
        year(int): select the transformer to be applied
    Returns:
        a dataframe in the standard format

    """

    if year == 2015:
        print(f"Transforming {filepath} to standard format...")
        return transform_2015_file_to_std_format(filepath)

    elif year == 2021:
        print(f"Transforming {filepath} to standard format...")
        return transform_2021_file_to_std_format(filepath)


def standardize_colnames(df, name_replacements):
    colnames = df.columns
    df.columns = [name_replacements[colname] for colname in colnames]


def transform_2015_file_to_std_format(filepath):
    # TODO: add fecha_hora_actualizaciop column
    all_dfs = pd.read_excel(filepath, sheet_name=None, engine=EXCEL_ENGINE)
    sheets = []
    for y in range(len(all_dfs.keys())):
        df = pd.read_excel(filepath, sheet_name=y, engine=EXCEL_ENGINE)
        # lowecase colnames of sheets
        lower_case_colnames(df)
        sheets.append(df)

    xl = pd.ExcelFile(filepath, engine=EXCEL_ENGINE)
    nombre_tipo = xl.sheet_names

    for i in range(len(sheets)):
        print(f"Transforming sheet: {nombre_tipo[i]}...")
        cols = list(sheets[i])
        cols.insert(0, cols.pop(cols.index("validos")))
        sheets[i] = sheets[i].loc[:, cols]
        cols.insert(0, cols.pop(cols.index("validos_real")))
        sheets[i] = sheets[i].loc[:, cols]
        if sheets[i]["codigomesa"].isnull().values.any():
            res = sheets[i][sheets[i]["codigomesa"].isna()]
            ite = 0
            for index, row in res.iterrows():
                if row.isna().sum() > (len(sheets[i].columns)) * 0.7:
                    ind = index + ite
                    print("NULLS EN CODIGO MESA (NO Error, advertencia!):")
                    # print(sheets[i].iloc[ind, :])
                    sheets[i].drop(ind, 0, inplace=True)
                    ite += -1
        sheets[i] = pd.melt(
            sheets[i],
            id_vars=cols[:10],
            value_vars=cols[10:],
            var_name="partido",
            value_name="cantidad",
        )
        sheets[i]["tipo_de_voto"] = nombre_tipo[i][6:].lower()
    result = pd.concat(sheets)
    result.loc[:, "pais"] = "bolivia"
    result.loc[:, "fecha_hora_descarga"] = datetime.today()
    result.loc[:, "fecha_hora_actualizacion"] = datetime.today()
    result.loc[:, "vuelta"] = 1
    result.loc[:, "tipo_de_voto"] = result.loc[:, "tipo_de_voto"].replace(
        {
            "a_territorio": "asambleista territorio",
            "a_poblacion": "asambleista poblacion",
            "e_s_desarrollo": "ejecutivo desarrollo",
            "a_regional": "asambleista regional",
            "gobernador_vicegobernador": "gobernador vicegobernador",
        }
    )
    result = result.rename(
        columns={
            "validos_real": "validos_real",
            "validos": "validos",
            "codigomesa": "mesa_pk",
            "nomdep": "nombre_departamento",
            "nomprov": "nombre_provincia",
            "nommun": "nombre_municipio",
            "nomreci": "nombre_recinto",
            "numeromesa": "numero",
            "idtipoacta": "id_tipo_acta",
            "inscritoshabilitados": "inscritos",
        }
    )
    return result


def transform_2021_file_to_std_format(filepath):
    date_cut = filepath.partition("ESN2021")[2]
    date_act = (
        date_cut[4:8]
        + "-"
        + date_cut[8:10]
        + "-"
        + date_cut[10:12]
        + " "
        + date_cut[13:15]
        + ":"
        + date_cut[15:17]
        + ":"
        + date_cut[17:19]
    )
    date_dow = str(time.ctime(os.path.getctime(filepath)))
    print(filepath)
    all_dfs = pd.read_excel(filepath, sheet_name=None, engine=EXCEL_ENGINE)
    sheets = []
    for y in range(len(all_dfs.keys())):
        sheets.append(
            pd.read_excel(filepath, sheet_name=y, engine=EXCEL_ENGINE)
        )

    xl = pd.ExcelFile(filepath, engine=EXCEL_ENGINE)
    nombre_tipo = xl.sheet_names
    for i in range(len(sheets)):
        cols = list(sheets[i])
        cols.insert(0, cols.pop(cols.index("VOTO_VALIDO")))
        sheets[i] = sheets[i].loc[:, cols]
        cols.insert(0, cols.pop(cols.index("VOTO_EMITIDO")))
        sheets[i] = sheets[i].loc[:, cols]
        cols.insert(0, cols.pop(cols.index("VOTO_VALIDO_SISTEMA")))
        sheets[i] = sheets[i].loc[:, cols]
        cols.insert(0, cols.pop(cols.index("VOTO_EMITIDO_SISTEMA")))
        sheets[i] = sheets[i].loc[:, cols]
        if sheets[i]["CODIGO_MESA"].isnull().values.any():
            res = sheets[i][sheets[i]["CODIGO_MESA"].isna()]
            ite = 0
            for index, row in res.iterrows():
                if row.isna().sum() > (len(sheets[i].columns)) * 0.7:
                    ind = index + ite
                    print("NULLS EN CODIGO MESA (NO Error, advertencia!):")
                    print(sheets[i].iloc[ind, :])
                    sheets[i].drop(ind, 0, inplace=True)
                    ite += -1
        sheets[i] = pd.melt(
            sheets[i],
            id_vars=cols[:20],
            value_vars=cols[20:],
            var_name="partido",
            value_name="cantidad",
        )
        sheets[i]["tipo_de_voto"] = nombre_tipo[i].lower()
        sheets[i]["fecha_hora_actualizacion"] = date_act
        sheets[i]["fecha_hora_descarga"] = date_dow
        sheets[i]["vuelta"] = 1
    result = pd.concat(sheets)
    result = result.rename(
        columns={
            "VOTO_EMITIDO_SISTEMA": "voto_emitido_sistema",
            "VOTO_VALIDO_SISTEMA": "voto_valido_sistema",
            "VOTO_EMITIDO": "voto_emitido",
            "VOTO_VALIDO": "voto_valido",
            "CODIGO_MESA": "mesa_pk",
            "DESCRIPCION": "descripcion",
            "CODIGO_PAIS": "codigo_pais",
            "NOMBRE_PAIS": "nombre_pais",
            "CODIGO_DEPARTAMENTO": "codigo_departamento",
            "NOMBRE_DEPARTAMENTO": "nombre_departamento",
            "NOMBRE_PROVINCIA": "nombre_provincia",
            "CODIGO_SECCION": "codigo_seccion",
            "NOMBRE_MUNICIPIO": "nombre_municipio",
            "CODIGO_LOCALIDAD": "codigo_localidad",
            "NOMBRE_LOCALIDAD": "nombre_localidad",
            "CODIGO_RECINTO": "codigo_recinto",
            "NOMBRE_RECINTO": "nombre_recinto",
            "NUMERO_MESA": "numero",
            "INSCRITOS_HABILITADOS": "inscritos",
        }
    )
    result["fecha_hora_descarga"] = pd.to_datetime(result.fecha_hora_descarga)
    result["fecha_hora_descarga"] = result["fecha_hora_descarga"].dt.strftime(
        "%Y-%m-%d %H:%M:%S"
    )
    result.loc[:, "tipo_de_voto"] = result.loc[:, "tipo_de_voto"].replace(
        {
            "asambleista por territorio": "asambleista territorio",
            "asambleista por poblacion": "asambleista poblacion",
        }
    )
    return result


# %%
# Test 2015 standard transformer
if __name__ == "__main__":
    folder_path = "G:/docs_adrian/bambootec/election_data/subnational_elections_2021/dataset_2015"
    filenames = os.listdir(folder_path)
    filepaths = [os.path.join(folder_path, filename) for filename in filenames]

    # Get the number of sheets
    test_filepath = filepaths[1]
    test_xl = pd.ExcelFile(test_filepath, engine=EXCEL_ENGINE)
    n_sheets = len(test_xl.sheet_names)
    print(f"The number of sheets is: {n_sheets}")

    # Read file
    test_dfs_dict = pd.read_excel(
        test_filepath,
        sheet_name=[x for x in range(0, n_sheets)],
        engine=EXCEL_ENGINE,
    )
    test_dfs_list = list(test_dfs_dict.values())
    test_dfs_list_validos = [df["Validos_Real"] for df in test_dfs_list]
    test_validos_series = pd.concat(test_dfs_list_validos)
    total_validos_orig = test_validos_series.sum()
    print(f"El total de votos validos en el archivo es: {total_validos_orig}")

    standardized_test_df = transform_2015_file_to_std_format(test_filepath)
    total_validos_std = standardized_test_df[
        (standardized_test_df["partido"] != "blancos")
        & (standardized_test_df["partido"] != "nulos")
    ]["cantidad"].sum()
    print(f"Nombre of records standardized {standardized_test_df.shape[0]}")
    print(f"El total de votos validos en el archivo es: {total_validos_std}")
    assert (
        total_validos_orig == total_validos_std
    ), "la función de formato standard no retorna la cantidad de votos validos original"
    print("Test del módulo standard transformer: aprobado")

# %%
