# %%
import pandas as pd
import sqlalchemy

# import mysql.connector
from sqlalchemy import create_engine

con = create_engine(
    "postgresql://electionData:electionDataPassword@34.122.17.115:5000/subnationals2021production",
    pool_recycle=3600,
    pool_timeout=57600,
)

# %%


# def mayor_percentage_plot(con):

query = """
SELECT municipio_pk, fecha_hora_actualizacion, sum(cantidad) as total_de_votos_mun FROM public.votos
where tipo_de_voto = 'alcalde' and partido_pk NOT IN ( 8, 9 )
group by municipio_pk, fecha_hora_actualizacion
"""
mun_total_votes_df = pd.read_sql_query(query, con)

query = """
select municipio_pk, departamento_pk, partido_pk, sum(cantidad) as cantidad_partido, fecha_hora_actualizacion from public.votos
where tipo_de_voto = 'alcalde' AND partido_pk not in ( 8, 9 )
group by partido_pk, municipio_pk, departamento_pk,  fecha_hora_actualizacion	
"""
mun_party_votes_df = pd.read_sql_query(query, con)

query = (
    """select departamento_pk, nombre as nombre_departamento from public.departamento"""
)
department_df = pd.read_sql_query(query, con)

query = """select municipio_pk, nombre as nombre_municipio from public.municipio"""
mun_df = pd.read_sql_query(query, con)

query = """select partido_pk, nombre as nombre_partido from public.partido"""
partido_df = pd.read_sql_query(query, con)

percentage_df = mun_party_votes_df.join(
    mun_total_votes_df.set_index(["municipio_pk", "fecha_hora_actualizacion"]),
    on=["municipio_pk", "fecha_hora_actualizacion"],
)

percentage_df = percentage_df.join(
    department_df.set_index(["departamento_pk"]), on=["departamento_pk"]
)

percentage_df = percentage_df.join(
    mun_df.set_index(["municipio_pk"]), on=["municipio_pk"]
)
percentage_df = percentage_df.join(
    partido_df.set_index(["partido_pk"]), on=["partido_pk"]
)

#%%
percentage_df = percentage_df[
    [
        "nombre_municipio",
        "nombre_departamento",
        "nombre_partido",
        "cantidad_partido",
        "total_de_votos_mun",
        "fecha_hora_actualizacion",
    ]
]

#%%
percentage_df = percentage_df.astype(
    {"cantidad_partido": float, "total_de_votos_mun": float}
)
#%%
percentage_df["porcentaje_partido"] = (
    percentage_df["cantidad_partido"] / percentage_df["total_de_votos_mun"]
)
#%%
percentage_df["porcentaje_partido"] = percentage_df["porcentaje_partido"] * 100
# %%
percentage_df = percentage_df.astype(
    {"cantidad_partido": int, "total_de_votos_mun": int}
)
# df = mayor_percentage_plot(con)

# %%
percentage_df
# %%
