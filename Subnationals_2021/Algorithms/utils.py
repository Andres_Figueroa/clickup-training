# %%
import pandas as pd
from sqlalchemy import create_engine
from functools import reduce
import openpyxl

con = create_engine(
    "postgresql://electionData:electionDataPassword@34.122.17.115:5000/subnationals2021production", pool_recycle=3600, pool_timeout=57600)
EXCEL_ENGINE = "openpyxl"

# %%


def get_elecciones_pk(year, con):
    YEAR_COLNAME = "anio"
    ELECCIONES_TABLENAME = "elecciones"
    elec_df = get_ids_db(ELECCIONES_TABLENAME, [YEAR_COLNAME], con)
    return elec_df[elec_df[YEAR_COLNAME] == year][ELECCIONES_TABLENAME+"_pk"][0]


def get_ids_db(table_name, col_ids_db, con):
    # TODO: more efficient implementation using SQL WHERE
    """Gets the ids baesd on the columns that make the record unique

    Args: 
        table_name (str): name of the table in the db
        col_ids_db (:obj:`list`): columns that make the records unique
        con (:obj:`sqlalchemy.engineEngine`): db connection
    Returns
        A dataframe with the pk and the col_ids
    """

    columns = [table_name+"_pk"]+col_ids_db
    return pd.read_sql_table(table_name, con, columns=columns).drop_duplicates()


def delete_table_data(table_name, con):
    con.execute(f"DELETE FROM {table_name}")


def get_id_cols_from_table(table_name, nationals=False):
    if table_name == "municipio":
        col_ids = ["nombre_municipio", "nombre_departamento"]
    elif table_name == "departamento":
        col_ids = ["nombre_departamento"]
    elif table_name == "recinto":
        col_ids = ["nombre_recinto", "nombre_municipio", "nombre_departamento"]
    else:
        raise ValueError(f"table {table_name} not handled")
    if nationals:
        return col_ids + ["pais"]
    else:
        return col_ids


def pk_2_std_format_colnames(colnames):
    def pk_2_std_format_colname(colname):
        return "nombre_" + colname.split("_")[0]
    if isinstance(colnames, list):
        return [pk_2_std_format_colname(cln) for cln in colnames]
    elif isinstance(colnames, str):
        return pk_2_std_format_colname(colnames)
    else:
        raise TypeError(f"Cannot handle type {type(colnames)}")


def std_format_2_pk_colnames(colnames):
    def std_format_pk_2_colname(colname):
        return colname.split("_")[1]+"_pk"
    if isinstance(colnames, list):
        return [std_format_pk_2_colname(cln) for cln in colnames]
    elif isinstance(colnames, str):
        return std_format_pk_2_colname(colnames)
    else:
        raise TypeError(f"Cannot handle type {type(colnames)}")


def get_table_name_from_pk_colnames(colnames):
    return colnames.split("_")[0]


def get_table_name_from_std_format_colnames(colnames):
    return colnames.split("_")[1]


def replace_names_with_pks(df, table_name, con, nationals=False):
    # TODO: implement different countries for nationals
    # Will only receive munipcio and recinto as table names
    NAME = "nombre"
    id_colnames = get_id_cols_from_table(table_name)
    print(
        f"Replacing names: {id_colnames} from table {table_name} with pks (primary_keys)...")
    ids_df = df.loc[:, id_colnames].drop_duplicates()
    ids_pks_df = add_pks_to_names(
        ids_df, con, id_colnames, nationals)
    print("Primary keys replaced")
    return df.merge(ids_pks_df, on=id_colnames+[NAME+"_"+table_name], how="inner").drop(id_colnames, axis=1)


def add_pks_to_names(df,  con, id_colnames, nationals=False):
    NAME = "nombre"
    current_colname = id_colnames[0]
    print(current_colname)
    current_tb_name = current_colname.split("_")[1]
    if current_tb_name == "departamento":
        colnames_db = std_format_2_pk_colnames(id_colnames)+[NAME]
        print(f"Obtaining table {current_tb_name}...")
        db_df = pd.read_sql_table(current_tb_name, con, columns=colnames_db)
        db_df = db_df.rename({NAME: NAME+"_"+current_tb_name},
                             axis=1).drop_duplicates()
        print(
            f"{db_df.shape[0]} lines of table {current_tb_name} were obtained")
        pks_df = df.merge(db_df, on=id_colnames, how="inner")
        return pks_df
    else:
        reduced_id_colnames = id_colnames[1:]
        colnames_db = std_format_2_pk_colnames(id_colnames)+[NAME]
        reduced_ids_df = df.loc[:, reduced_id_colnames].drop_duplicates()
        print(f"Reduced id dataframe columns: {reduced_ids_df.columns}")
        reduced_ids_pks_df = add_pks_to_names(
            reduced_ids_df, con, reduced_id_colnames, nationals)
        print(
            f"Reduced id pks dataframe columns: {reduced_ids_pks_df.columns}")
        db_df = pd.read_sql_table(current_tb_name, con, columns=colnames_db)
        db_df = db_df.rename(
            {NAME: NAME+"_"+current_tb_name}, axis=1).drop_duplicates()
        pks_df = reduced_ids_pks_df.merge(
            db_df, on=std_format_2_pk_colnames(reduced_id_colnames), how="inner")

        return pks_df


# %%
if __name__ == "__main__":
    dpto_ids_df = get_ids_db("departamento", ["pais", "nombre"], con)
    print(dpto_ids_df)

# %%
#print("elecciones", get_elecciones_pk(2015, con))

# %%
