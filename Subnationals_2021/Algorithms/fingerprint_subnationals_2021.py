# %%
import pandas as pd
from algorithms import *
from utils import *
from standard_form_transformer import *
# %%
# Use db: 1; use test file: 0
db = 1
if not db:
    DESC_GOB = "GOBERNADOR(A)"
    DESC_ALC = "ALCALDESA/ALCALDE"
    FILENAME = "./ESN2021_OR_20210315_204236_886064895172685586.csv"
    dfs = [pd.read_csv(FILENAME, encoding="latin_1", sep="|")]

    PARTIDO_COLNAMES = ['APU', 'BST', 'C-A', 'FPV', 'FIC', 'INCA-FS', 'TCA', 'L.E.A.L',
                        'MAS-IPSP', 'MRP', 'MTS', 'PP', 'PAN-BOL', 'PDC', 'SUMA', 'UCS',
                        'UNICO', 'UN SOL PARA ORURO']
    DEN_QTY_COL = 'cantidad_votos_validos'
    df = dfs[0].melt(set(dfs[0].columns).difference(set(PARTIDO_COLNAMES)),
                     PARTIDO_COLNAMES, var_name='partido', value_name=DEN_QTY_COL)

    df
    # filter only gobernador and alcalde votes
    df = df.loc[(df["DESCRIPCION"] == DESC_GOB) |
                (df["DESCRIPCION"] == DESC_ALC), :]
# %%
tipo_voto = 'subgobernador'
if db:
    df = get_data_from_db(9, tipo_voto)
    DEN_QTY_COL = 'cantidad'

# %%
df
# %%
# Calculate party vote percentage
#GROUP_COLS = ["mesa_pk", "DESCRIPCION"]
GROUP_COLS = ["mesa_pk"]
AGG_COL = "cantidad_de_votos_total"
PERC_PREFIX = "porcentaje_"
PART_COL = "participacion"
SUSCRIBED_COL = "inscritos"
with_percentages_df = get_percentage_cols(df, GROUP_COLS, DEN_QTY_COL, [
                                          DEN_QTY_COL], agg_colname=AGG_COL, percentage_prefix=PERC_PREFIX)

# %%
with_percentages_df
# %%
with_percentages_df.loc[:, PERC_PREFIX +
                        DEN_QTY_COL] = with_percentages_df.loc[:, PERC_PREFIX+DEN_QTY_COL].round()
# %%
# Calculate participation
with_percentages_df.loc[:, PERC_PREFIX+PART_COL] = (
    with_percentages_df.loc[:, AGG_COL]*100/with_percentages_df.loc[:, SUSCRIBED_COL]).round()

# %%
with_percentages_df
# %%
vote_type_groups = with_percentages_df.groupby("tipo_de_voto", as_index=False)

# %%
# Split alcalde and gobernador
gob_df = [df for k, df in vote_type_groups if k == tipo_voto][0]
#alc_df = [df for k, df in vote_type_groups if k == DESC_ALC][0]
# %%
if gob_df.isnull().values.any():
    gob_df = gob_df.fillna(0)

# %%
DPTO_COL = "nombre_departamento"
PARTY_COL = "nombre_partido"
partido_dpto_groups = gob_df.groupby([DPTO_COL, PARTY_COL], as_index=False)
# %%
partido_dpto_groups
# %%
PREFIX = "subgob_"
sum_mesas = 0
VMIN = 0
VMAX = 40
center = (VMAX + VMIN) / 2
for ((dpto, partido), df) in partido_dpto_groups:
    fgp_df, vote_units_df, fgp_fig = plot_fingerprint(
        df,
        PERC_PREFIX+DEN_QTY_COL,
        PERC_PREFIX+PART_COL,
        unit="Mesa",
        title=f"Departamento: {dpto} Partido: {partido.upper()}",
        vmin=VMIN,
        center=center,
        vmax=VMAX,
        cmap='rocket'
    )
    sum_mesas = sum_mesas + fgp_df.values.sum()
    fgp_fig.savefig(
        f"./{PREFIX[:-1]}/{PREFIX}fingerprint_{dpto}_{partido}.png", dpi=1200, format="png")
    vote_units_df.to_csv(
        f"./{PREFIX[:-1]}/{PREFIX}vote_units_{dpto}_{partido}.csv", index=False)
# %%
DPTO_COL = "NOMBRE_DEPARTAMENTO"
MUN_COL = "NOMBRE_MUNICIPIO"
partido_mun_groups = alc_df.groupby(
    [DPTO_COL, MUN_COL, PARTY_COL], as_index=False)
# %%
PREFIX = "alc_"
sum_mesas = 0
VMIN = 0
VMAX = 40
center = (VMAX + VMIN) / 2
for ((dpto, mun, partido), df) in partido_mun_groups:
    fgp_df, vote_units_df, fgp_fig = plot_fingerprint(
        df,
        PERC_PREFIX+DEN_QTY_COL,
        PERC_PREFIX+PART_COL,
        unit="Mesa",
        title=f"Departamento: {dpto} Municipio: {mun} Partido: {partido.upper()}",
        keep_cols=["NOMBRE_MUNICIPIO", "NOMBRE_RECINTO"],
        vmin=VMIN,
        center=center,
        vmax=VMAX,
    )
    sum_mesas = sum_mesas + fgp_df.values.sum()
    fgp_fig.savefig(
        f"./{PREFIX[:-1]}/{PREFIX}fingerprint_{dpto}_{mun}_{partido}.png", dpi=1200, format="png")
    vote_units_df.to_csv(
        f"./{PREFIX[:-1]}/{PREFIX}vote_units_{dpto}_{mun}_{partido}.csv", index=False)

# %%
