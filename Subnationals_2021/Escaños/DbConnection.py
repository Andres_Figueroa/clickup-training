from sqlalchemy import MetaData, create_engine
from sqlalchemy.orm import sessionmaker
import pandas as pd
import psycopg2
import string
#conn_str ="postgresql://electionData:electionDataPassword@34.122.17.115:5000/electionData2020"
conn_str ="postgresql://electionData:electionDataPassword@34.122.17.115:5000/subnationals2021production"
engine = create_engine(conn_str,pool_size=20,max_overflow=0)

def get_elecciones():
    table = pd.read_sql_table(
        'elecciones',
        con=engine
    )
    return table

def get_municipio():
    table = pd.read_sql_table(
        'municipio',
        con=engine
    )
    return table

def get_partido():
    table = pd.read_sql_table(
        'partido',
        con=engine
    )
    return table

def get_municipio_variante_en_tiempo():
    table = pd.read_sql_table(
        'municipio_variante_en_tiempo',
        con=engine
    )
    return table

def get_departamento_variante_en_tiempo():
    table = pd.read_sql_table(
        'departamento_variante_en_tiempo',
        con=engine
    )
    return table

def get_departamento():
    table = pd.read_sql_table(
        'departamento',
        con=engine
    )
    return table

def get_votos():
    table = pd.read_sql_table(
        'votos',
        con=engine
    )
    return table

def get_recinto():
    table = pd.read_sql_table(
        'recinto',
        con=engine
    )
    return table

def get_mesa():
    table = pd.read_sql_table(
        'mesa',
        con=engine
    )
    return table

def get_resultadobase():
    table = pd.read_sql_query(
        'select departamento_pk,municipio_pk,tipo_de_voto,partido_pk, sum(cantidad) as votos from votos where departamento_pk=20  and  municipio_pk=26 and tipo_de_voto='+ "'"+'presidencial'+"'"+' and fecha_hora_actualizacion='+"'"+'"'+'2020-10-24 11:48:00'+'"'+"'"+' and partido_pk not in (14,15) group by departamento_pk,municipio_pk,tipo_de_voto,partido_pk order by votos desc',
        con=engine
    )
    return table

def get_resultado_geo(ptipo_voto,p_nomdep,p_nommun):
    table = pd.read_sql_query(
        'select * from get_resultado_geo('+"'"+ptipo_voto+"'"+','+"'"+p_nomdep+"'"+','+"'"+p_nommun+"'"+')',
        con=engine
    )
    return table

def get_resultado_geo_dep(ptipo_voto,p_nomdep):
    table = pd.read_sql_query(
        'select * from get_resultado_geo_dep('+"'"+ptipo_voto+"'"+','+"'"+p_nomdep+"'"+')',
        con=engine
    )
    return table

def get_resultado_geo_dep_sql(ptipo_voto,p_nomdep):
   
    fechadep=get_fecha_dep(p_nomdep)
    table = pd.read_sql_query(
        #'select dep.nombre as nombre_departamento , vot.departamento_pk,vot.tipo_de_voto,op.nombre as sigla_op,vot.partido_pk, sum(vot.cantidad) as votos from public.votos vot inner join public.departamento dep on dep.departamento_pk= vot.departamento_pk inner join public.partido op on op.partido_pk=vot.partido_pk where dep.nombre='+"'"+p_nomdep+"'"+' and vot.tipo_de_voto::varchar = '+"'"+ptipo_voto+"'"+' and vot.fecha_hora_actualizacion='+"'"+'"'+'2021-03-15 23:23:38'+'"'+"'"+' and vot.partido_pk not in (8,9) group by dep.nombre,vot.departamento_pk,vot.tipo_de_voto,op.nombre,vot.partido_pk order by votos desc',
        'select dep.nombre as nombre_departamento , vot.departamento_pk,vot.tipo_de_voto,op.nombre as sigla_op,vot.partido_pk, sum(vot.cantidad) as votos from public.votos vot inner join public.departamento dep on dep.departamento_pk= vot.departamento_pk inner join public.partido op on op.partido_pk=vot.partido_pk where dep.nombre='+"'"+p_nomdep+"'"+' and vot.tipo_de_voto::varchar = '+"'"+ptipo_voto+"'"+' and vot.fecha_hora_actualizacion='+"'"+'"'+fechadep+'"'+"'"+' and vot.partido_pk not in (8,9) group by dep.nombre,vot.departamento_pk,vot.tipo_de_voto,op.nombre,vot.partido_pk order by votos desc',
        con=engine
    )
    return table

def get_resultado_geo_sql(ptipo_voto,p_nomdep,p_nommun):
    fechadep=get_fecha_dep(p_nomdep)
    table = pd.read_sql_query(
        'select dep.nombre as nombre_departamento , vot.departamento_pk,mun.nombre as nombre_municipio,vot.municipio_pk,vot.tipo_de_voto,op.nombre as sigla_op,vot.partido_pk, sum(vot.cantidad) as votos from public.votos vot inner join public.municipio mun on mun.municipio_pk=vot.municipio_pk inner join public.departamento dep on dep.departamento_pk= vot.departamento_pk inner join public.partido op on op.partido_pk=vot.partido_pk where dep.nombre='+"'"+p_nomdep+"'"+' and  mun.nombre='+"'"+p_nommun+"'"+' and vot.tipo_de_voto::varchar = '+"'"+ptipo_voto+"'"+' and vot.fecha_hora_actualizacion='+"'"+'"'+fechadep+'"'+"'"+' and vot.partido_pk not in (8,9) group by dep.nombre,vot.departamento_pk,mun.nombre,vot.municipio_pk,vot.tipo_de_voto,op.nombre,vot.partido_pk order by votos desc',
        con=engine
    )
    return table
#configuracion ultimas fechas para consulta de la bd
def get_fecha_dep(p_nomdep): 
    fecha=""
    if p_nomdep=='chuquisaca':
        fecha="2021-03-15 23:23:38" 
    elif p_nomdep=='la paz':
        fecha="2021-03-15 23:24:01"
    elif p_nomdep=='cochabamba':
        fecha="2021-03-15 23:25:10"
    elif p_nomdep=='oruro':
        fecha="2021-03-15 23:25:38"
    elif p_nomdep=='potosi':
        fecha="2021-03-15 23:25:47"
    elif p_nomdep=='tarija':
        fecha="2021-03-15 23:25:59"
    elif p_nomdep=='santa cruz':
        fecha="2021-03-15 23:26:11"
    elif p_nomdep=='beni':
        fecha="2021-03-15 23:26:58"
    elif p_nomdep=='pando':
        fecha="2021-03-15 23:27:08"
    else:
        fecha=""
    
    return fecha