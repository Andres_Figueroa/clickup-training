import pandas as pd
import sqlalchemy

# ---------------------------------------------------------------------------- #
#                              General guidelines
# - Add as many functions as necessary
# - Change the function definition if convenient and necessary (also the comments)
# - Try to make the code as reusable as possible (general functions)
# - Try to do the code as clean as posible
# - Dataframe variables end with "_df" (E.g. votes_df)
# - Comment the code in the way that is shown here
# - Follow the style shown in the template
# - Test the main function with a small dataset
# (E.g. variables in snake case (E.g. votes_per_day), clases CamelCase if any)
# ---------------------------------------------------------------------------- #

# %%


def compare_with_existing_records(df, con, table_name, col_ids):
    """Compares dataframe with existing db records returning the new ones



    Args:
        std_df (:obj:pd.DataFrame): A dataframe in the standard form
    Returns:
        a dataframe with the following characteristics:
            - Colnames: snake lower case singular (nombre_municipio). Keep a convention
            - String cols:
                - keep the ñ's
                - remove all special characters (*,&,! etc)
                - remove all the head and trailing spaces
                - replace nulls with a "null" string or a convention if specified
                - from data exploration, replace equivalent names (challapa, chayapa are the same, pick challapa and replace all chayapa by challapa)
            - Float cols:
                - Asign the corresponding datatype in the dataframe
                - np.nan replaced by 0 or any convention specified
            - Int cols:
                - Asign the correspoding datatype in the dataframe
                - np.nan replaced by 0 or any convention specified
    """

    if table_name == 'municipio':
        municipio_sql = """select municipio as nombre_municipio, provincia as nombre_provincia,departamento as nombre_departamento from 
        (select municipio_pk, nombre as municipio, provincia, departamento_pk from municipio) as t1 
        inner join 
        (select nombre as departamento, pais,departamento_pk from departamento) as t2 
        on t1.departamento_pk = t2.departamento_pk where pais = 'bolivia'
        order by departamento,municipio"""
        df_db = pd.read_sql_query(municipio_sql, con).drop_duplicates()
        df_cl = df[col_ids].drop_duplicates()
        df_comp = pd.concat([df_cl, df_db, df_db]).drop_duplicates(keep=False, subset=col_ids)
        return df_comp
    elif table_name == 'partido':
        partido_sql = """select nombre as partido from partido"""
        df_db = pd.read_sql_query(partido_sql, con).drop_duplicates()
        df_cl = df[col_ids].drop_duplicates()
        df_comp = pd.concat([df_cl, df_db, df_db]).drop_duplicates(
            keep=False, subset=col_ids)
        return df_comp
    elif table_name == 'recinto':

        recinto_sql = """select recinto as nombre_recinto,municipio as nombre_municipio,departamento as nombre_departamento from
        (select recinto,municipio,departamento_pk from
        (select nombre as recinto,municipio_pk, departamento_pk from recinto) as t1
        inner join
        (select nombre as municipio, municipio_pk from municipio) as t2
        on t1.municipio_pk = t2.municipio_pk) as t3
        inner join
        (select departamento_pk,nombre as departamento from departamento) as t4
        on t3.departamento_pk = t4.departamento_pk"""
        df_db = pd.read_sql_query(recinto_sql, con).drop_duplicates()
        df_cl = df[col_ids].drop_duplicates()
        df_comp = pd.concat([df_cl, df_db, df_db]).drop_duplicates(
            keep=False, subset=col_ids)
        return df_comp
    elif table_name == 'departamento':
        depart_sql = """select nombre as nombre_departamento from departamento"""
        df_db = pd.read_sql_query(depart_sql, con).drop_duplicates()
        df_cl = df[col_ids].drop_duplicates()
        df_comp = pd.concat([df_cl, df_db, df_db]).drop_duplicates(keep=False, subset=col_ids)
        return df_comp
    elif table_name == "mesa":
        mesa_sql = """select mesa_pk from mesa"""
        df_db = pd.read_sql_query(mesa_sql, con).drop_duplicates()
        df_cl = df[col_ids].drop_duplicates()
        df_comp = pd.concat([df_cl, df_db, df_db]).drop_duplicates(
            keep=False, subset=col_ids)
        return df_comp
    elif table_name == "municipio_variante_en_tiempo":
        mesa_sql = """select municipio_pk, elecciones_pk, departamento_pk from municipio_variante_en_tiempo"""
        df_db = pd.read_sql_query(mesa_sql, con).drop_duplicates()
        df_cl = df[col_ids].drop_duplicates()
        df_comp = pd.concat([df_cl, df_db, df_db]).drop_duplicates(
            keep=False, subset=col_ids)
        return df_comp
    elif table_name == "departamento_variante_en_tiempo":
        mesa_sql = """select departamento_pk, elecciones_pk from departamento_variante_en_tiempo"""
        df_db = pd.read_sql_query(mesa_sql, con).drop_duplicates()
        df_cl = df[col_ids].drop_duplicates()
        df_comp = pd.concat([df_cl, df_db, df_db]).drop_duplicates(
            keep=False, subset=col_ids)
        return df_comp
    

# Add more functions here if required

# %%
