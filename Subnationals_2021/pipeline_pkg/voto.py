# %%
import pandas as pd
import numpy as np
import openpyxl
import os
from pipeline_pkg.comparison_with_existing_records import *
from pipeline_pkg.standard_form_transformer import transform_to_standard_format
from algorithms.utils import *
from pipeline_pkg.cleaning import clean_standard_form_df
from pipeline_pkg.upload import *

EXCEL_ENGINE = "openpyxl"
# %%


def fill_voto(std_df, con, year, chunksize=5000):
    COLS_STD_FORMAT = [
        "nombre_municipio",
        "nombre_recinto",
        "mesa_pk",
        "partido",
        "vuelta",
        "fecha_hora_descarga",
        "fecha_hora_actualizacion",
        "cantidad",
        "nombre_departamento",
        "tipo_de_voto",
    ]
    TABLE_NAME = "votos"
    TABLE_PARTIDO_NAME = "partido"
    COLS_IDS_PARTIDO = ["nombre", "elecciones_pk"]
    PK_PARTIDO_COLNAME = ["partido_pk"]
    SCHEMA = "public"

    print(
        "//////////////////////////// FILL VOTO ////////////////////////////"
    )
    print(f"Select votos relevant cols: {COLS_STD_FORMAT}")
    new_rel_df = std_df.loc[:, COLS_STD_FORMAT].drop_duplicates()
    print(f"After cleaning shape {new_rel_df.shape}")
    print(f"Before adding pks: {new_rel_df.columns}")
    new_rel_pks_records_df = replace_names_with_pks(new_rel_df, "recinto", con)
    print(f"After replacing pks: {new_rel_pks_records_df.shape}")
    new_rel_pks_records_df.loc[:, "elecciones_pk"] = get_elecciones_pk(
        year, con
    )
    partido_with_col_ids = get_ids_db(
        TABLE_PARTIDO_NAME, PK_PARTIDO_COLNAME + COLS_IDS_PARTIDO, con
    ).rename({"nombre": "partido"}, axis=1)
    print(f"Partido with id cols {partido_with_col_ids.columns}")
    print(f"Added pks {new_rel_pks_records_df.columns}")
    # Replace partido pks
    COLS_IDS_PARTIDO = [
        x.replace("nombre", "partido") for x in COLS_IDS_PARTIDO
    ]
    new_rel_pks_records_df = new_rel_pks_records_df.merge(
        partido_with_col_ids, on=COLS_IDS_PARTIDO, how="inner"
    ).drop("partido", axis=1)
    new_rel_pks_records_df.loc[:, "ruta_del_archivo_base"] = "r"
    new_rel_pks_records_df.loc[:, "tipo_de_computo"] = bytes(1)
    new_rel_pks_records_df.loc[:, "estado_de_acta"] = "en progreso"
    new_rel_pks_records_df.loc[:, "cantidad"] = new_rel_pks_records_df.loc[
        :, "cantidad"
    ].fillna(-1)
    print("After adding pks: ", new_rel_pks_records_df.columns)
    print(f"Load {new_rel_pks_records_df.shape[0]} records to votos...")
    try:
        append_to_db_table(
            new_rel_pks_records_df,
            con,
            TABLE_NAME,
            SCHEMA,
            chunksize=chunksize,
        )
    except Exception as e:
        with open("voto_error.log", "aw") as f:
            f.write(str(e))
    print("Load completed")
    print(
        "//////////////////////////// END FILL VOTO ////////////////////////////"
    )


# %%
if __name__ == "__main__":
    folderpath = "../../../dataset_2015"
    filenames = os.listdir(folderpath)
    test_filepath = os.path.join(folderpath, filenames[1])
    test_df = pd.read_excel(test_filepath, engine=EXCEL_ENGINE)
    std_test_df = transform_to_standard_format(test_filepath, 2015)
    clean_std_test_df = clean_standard_form_df(std_test_df)
    # Fails when using subset due to assert on size
    fill_voto(clean_std_test_df, con, 2015)
