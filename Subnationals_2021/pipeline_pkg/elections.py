import pandas as pd
import os
import datetime
import sqlalchemy
from pipeline_pkg.upload import append_to_db_table
from pipeline_pkg.utils import *
# %%


def year_already_inserted(year, con):
    query = """SELECT anio FROM public.elecciones"""

    obtained_elecciones_year_df = con.execute(query)

    for yeardb in obtained_elecciones_year_df:
        if int(yeardb[0]) == year:
            return True

    return False


def get_datetime_values(date_str, time_str):
    file_year = date_str[0:4]
    file_month = date_str[4:6]
    file_day = date_str[6:8]
    file_hour = time_str[0:2]
    file_min = time_str[2:4]
    file_sec = time_str[4:6]

    return(int(file_year),
           int(file_month),
           int(file_day),
           int(file_hour),
           int(file_min),
           int(file_sec))


def fill_elecciones(year=None, path='/workspaces/NewPipelineElectionData/Subnationals_2021/Pipeline/Bot/TEST_FILES', con=None, update_to_db=False, partido_ganador=-1, db_table_name='elecciones'):
    newest_file_name = 'NFS'
    if year is None:
        files = os.listdir(path)
        tmp = 0
        newest_file_datetime = datetime.datetime(1, 1, 1, 0, 0, 0)

        for csv in files:
            tmp += 1
            splited = csv.split('.')
            file_name_splited = splited[0].split('-')

            tmp_datetime = get_datetime_values(
                file_name_splited[1], file_name_splited[2])

            file_datetime_var = datetime.datetime(tmp_datetime[0],
                                                  tmp_datetime[1],
                                                  tmp_datetime[2],
                                                  tmp_datetime[3],
                                                  tmp_datetime[4],
                                                  tmp_datetime[5])

            if file_datetime_var > newest_file_datetime:
                newest_file_datetime = file_datetime_var
                newest_file_name = files[tmp-1]
                year = file_name_splited[0].split(
                    "_")[len(file_name_splited[0].split("_"))-1]

    inserted = year_already_inserted(year, con)

    if (update_to_db) and (not inserted):

        upload_df = pd.DataFrame(
            {
                'anio': [int(year)],
                'partido_ganador_pk': partido_ganador
            },
            index=range(1),
        )
        print("Estableciendo Conexion")
        if con is None:
            database_username = 'electionData'
            database_password = 'electionDataPassword'
            database_ip = '34.122.17.115:5000'
            database_name = 'electionData2020'
            database_connection = sqlalchemy.create_engine('postgresql://{0}:{1}@{2}/{3}'.
                                                           format(database_username,
                                                                  database_password,
                                                                  database_ip,
                                                                  database_name),
                                                           pool_recycle=1,
                                                           pool_timeout=57600).connect()
        elif con is not None:
            database_connection = con
        print("Insertando")

        print(upload_df)

        success = append_to_db_table(df=upload_df,
                                     con=database_connection,
                                     table_name=db_table_name,
                                     schema='electionData2020',
                                     chunksize=1)

        print('was it successful? ' + str(success))

        # database_connection.close()

    elif inserted:
        newest_file_name = 'year already inserted'

    return(newest_file_name, int(year))


# fill_elecciones(update_to_db=True)
# %%
