from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import requests
import time

from datetime import datetime


from flask import Flask
app = Flask(__name__)


def enable_download_headless(browser,download_dir):
    browser.command_executor._commands["send_command"] = ("POST", '/session/$sessionId/chromium/send_command')
    params = {'cmd':'Page.setDownloadBehavior', 'params': {'behavior': 'allow', 'downloadPath': download_dir}}
    browser.execute("send_command", params)

@app.route('/')
def scraper():
    options = Options()
    options.headless = True
    options.add_argument("--window-size=1920,1200")
    options.add_argument("--no-sandbox")
    options.add_argument('--disable-dev-shm-usage')
    options.add_argument("--disable-notifications")
    options.add_argument("--headless")
    options.add_argument('--verbose')
    options.add_experimental_option("prefs", {
            #'chrome.page.customHeaders.referrer' : 'https://www.oep.org.bo/',
            "download.default_directory": "./DOWNLOADED_FILES",
            "download.prompt_for_download": False,
            "download.directory_upgrade": True,
            "safebrowsing_for_trusted_sources_enabled": False,
            "safebrowsing.enabled": False
    })
    options.add_argument('--disable-gpu')
    options.add_argument('--disable-software-rasterizer')
    options.add_argument("--disable-blink-features=AutomationControlled")
    options.add_experimental_option("excludeSwitches", ["enable-automation"])
    options.add_experimental_option('useAutomationExtension', False)
    options.add_argument("user-agent=Mozilla/5.0 (X11: Linux x86_64) Chrome/89.0.4389.82 Safari/537.36")

    #Mozilla/5.0 (Windows Phone 10.0; Android 4.2.1; Microsoft; Lumia 640 XL LTE) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.135 Mobile Safari/537.36 Edge/12.10166"
    
    DRIVER_PATH = '/usr/bin/chromedriver'
    download_folder = "./DOWNLOADED_FILES"

    #enable_download_headless(driver, download_dir)
    driver = webdriver.Chrome(options=options, executable_path=DRIVER_PATH)

    enable_download_headless(driver, download_folder)
    time.sleep(1)

    
    pageURL="https://www.oep.org.bo/"
    driver.get(pageURL)
    print("Scraping: " + driver.current_url)
    #time.sleep(60)
    #next_arrow = driver.find_element_by_xpath("//li//a[@class='flex-next']")
    #next_arrow.click()
    #print("clicked on arrow")
    entrada = driver.find_elements_by_xpath("//ul[@class='slides']//li//a")
    newPageURL = entrada[1].get_attribute('href')
    #entrada[1].click()
    driver.get(newPageURL)
    print('Navigating to...{0}'.format(newPageURL))
    
    time.sleep(15)
    print(driver.page_source)

    time.sleep(1)

    title = driver.find_element_by_xpath("//h1")
    print(title.text)
    time.sleep(1)


    departamento_dropdown = driver.find_element_by_xpath("//p-dropdown[@placeholder='DEPARTAMENTO']")
    departamento_dropdown.click()
    time.sleep(1)

    departamento = departamento_dropdown.find_element_by_xpath("//p-dropdown[@placeholder='DEPARTAMENTO']//p-dropdownitem")
    
    #print(departamentos[0])
    departamento.click()
    time.sleep(1)

    #for departamento in departamentos:  
     #   departamento.click()
    for i in range(9):
        downloadIcon = driver.find_element_by_xpath("//div[@title='Descargar']")
        downloadIcon.click()
        time.sleep(2)    
        tabs = driver.find_elements_by_xpath("//div[@role='tab']")
        tabs[1].click()
        time.sleep(2)    
        departamentos_dropdown = driver.find_elements_by_xpath("//p-dropdown[@placeholder='DEPARTAMENTO']")
        departamentos_dropdown[3].click()
        time.sleep(2)
        download_departamentos = driver.find_elements_by_xpath("//p-dropdownitem")
        download_departamentos[i].click()
        time.sleep(2)
        download_button = driver.find_element_by_xpath("//button//span[text()=' DESCARGAR ']")
        download_button.click()
        time.sleep(2)
        end_button = driver.find_element_by_xpath("//button//span[text()='ENTENDIDO!']")
        end_button.click()
        print('Archivo numero {0} descargado'.format(i))
        time.sleep(15)
        
    
    print("\n")
    #Download Section
    #url = downloadButton.get_attribute('href')
    #r = requests.get(url, allow_redirects=True)
    #timestr = time.strftime("%Y%m%d-%H%M%S")
    #title.replace(" ", "_")
    #fileName = title + '-' + timestr + fileExtension
    #open(fileName, 'wb').write(r.content)

    #print(button.text)
    #button.click()
    driver.quit()
    print("i've just finished running")
    
    myFile = open('log.txt', 'a') 
    myFile.write('\nScrapper Excecuted on ' + str(datetime.now()))
    
    return 'Bot retrieved files on {0} \n'.format(str(datetime.now()))

