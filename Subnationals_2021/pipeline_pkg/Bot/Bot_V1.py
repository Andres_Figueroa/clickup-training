from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import requests
import time



options = Options()
options.headless = True
options.add_argument("--window-size=1920,1200")
options.add_argument("--no-sandbox")
options.add_argument('--disable-dev-shm-usage')
options.add_argument("--disable-notifications")


DRIVER_PATH = '/usr/bin/chromedriver'
download_folder = "Descargas"

#enable_download_headless(driver, download_dir)
driver = webdriver.Chrome(options=options, executable_path=DRIVER_PATH)

for item in range(1,5):
   
   pageURL="http://atlaselectoral.oep.org.bo/#/subproceso/"+ str(item) +"/1/1/datos-abiertos"
   driver.get(pageURL)
   print("Scraping: " + driver.current_url)
   
   driver.refresh()
   time.sleep(1)
   
   title = driver.find_element_by_xpath("//h2")
   print(title.text)
   
   button = driver.find_elements_by_xpath("//a[@class='list-group-item list-group-item-action ng-star-inserted']")
   print("Cantidad de enlaces encontrados: " + str(len(button)))
   for linkNumber in range(len(button)):
      print("\tEnlace #"+ str(linkNumber + 1) +":\n\t\t" + button[linkNumber].get_attribute('href'))      

   print("\n")
   #Download Section
   #url = button.get_attribute('href')
   #r = requests.get(url, allow_redirects=True)
   #open('descarga.csv', 'wb').write(r.content)

   #print(button.text)
   #button.click()
   


driver.quit()