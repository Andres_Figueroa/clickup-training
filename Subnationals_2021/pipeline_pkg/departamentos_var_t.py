import pandas as pd
import numpy as np
import sqlalchemy
import mysql.connector

from pipeline_pkg.upload import append_to_db_table
from pipeline_pkg.comparison_with_existing_records import compare_with_existing_records

# %%


def get_poblacion_df_depvt():
    poblacion_df = pd.DataFrame(
        {
            'departamento':        ['chuquisaca', 'la paz', 'cochabamba', 'oruro', 'potosi', 'tarija', 'beni', 'pando', 'santa cruz'],
            'poblacion_edad_voto': [415816, 2023824, 1351321, 364201, 563712, 388223, 289183, 94901, 2126338],
            'poblacion':           [654035, 3023791, 2086930, 548537, 907686, 591828, 507095, 158676, 3363377]
        },
        index=[1, 2, 3, 4, 5, 6, 7, 8, 9]
    )

    return poblacion_df


def upload_to_db(upload_df, con, db_table_name='testTable'):
    print("Estableciendo Conexion")
    database_connection = con
    print("Insertando")

    success = append_to_db_table(df=upload_df,
                                 con=database_connection,
                                 table_name=db_table_name,
                                 schema='subnational2015',
                                 chunksize=1)

    #print('was it successful? ' + str(success))

    return success


def get_elecciones_pk_from_db_depvt(database_connection, year):

    query = """SELECT elecciones_pk FROM public.elecciones 
                WHERE anio = {0}""".format(year)

    obtained_elecciones_df = database_connection.execute(query)
    print('llegue')
    return_df = pd.DataFrame(
        {
            'elecciones_pk': [],
        },
    )

    id = 0
    for elecciones in obtained_elecciones_df:
        id += 1
        tmp_df = pd.DataFrame(
            {
                'elecciones_pk': elecciones[0]
            },
            index=[id],
        )
        return_df = return_df.append(tmp_df)
        # print(return_df)

    #return_df = 1
    return return_df


def get_departamentos_pks_from_db_depvt(database_connection=None):

    query = """SELECT departamento_pk, nombre FROM public.departamento
                WHERE pais like 'bolivia' 
                ORDER BY departamento_pk"""

    obtained_departamentos_df = database_connection.execute(query)

    return_df = pd.DataFrame(
        {
            'departamento_pk': [],
            'nombre': []
        },
    )

    id = 0
    for departamento in obtained_departamentos_df:
        id += 1
        tmp_df = pd.DataFrame(
            {
                'departamento_pk': departamento[0],
                'nombre': departamento[1]
            },
            index=[id],
        )
        return_df = return_df.append(tmp_df)
        # print(return_df)

    return return_df


def fill_departamentos_var_t(con, year, upload_table_name='testTable'):

    departamentos_pks = get_departamentos_pks_from_db_depvt(
        database_connection=con)
    #departamentos_pks = departamentos_pks.departamento_pk.unique()

    elecciones_pk = get_elecciones_pk_from_db_depvt(
        database_connection=con, year=year)

    poblacion_df = get_poblacion_df_depvt()

    columna_elecciones_pk = np.full(
        shape=len(departamentos_pks), fill_value=int(elecciones_pk.elecciones_pk[1]))

    departamentos_pks.insert(1, 'elecciones_pk', columna_elecciones_pk)

    upload_df = departamentos_pks.join(
        poblacion_df.set_index('departamento'), on='nombre')
    print(upload_df)
    upload_df = upload_df.drop(['nombre'], axis=1)

    clean_df = compare_with_existing_records(
        upload_df, con, 'departamento_variante_en_tiempo', ['departamento_pk', 'elecciones_pk'])
    if clean_df is not None and not clean_df.empty:
        upload_df = clean_df.join(upload_df.set_index(
            ['departamento_pk', 'elecciones_pk']), on=['departamento_pk', 'elecciones_pk'])

        upload_df = upload_df[['departamento_pk',
                               'elecciones_pk', 'poblacion_edad_voto', 'poblacion']]

        return upload_to_db(upload_df=upload_df, con=con, db_table_name=upload_table_name)
    else:
        print("No new departamentos variantes en el tiempo...")


# %%
"""
database_username = 'electionData'
database_password = 'electionDataPassword'
database_ip       = '34.122.17.115:5000'
database_name     = 'electionData2020'
database_connection = sqlalchemy.create_engine('postgresql://{0}:{1}@{2}/{3}'.
                                            format(database_username,
                                                    database_password, 
                                                    database_ip,
                                                    database_name),
                                            pool_recycle=1,
                                            pool_timeout=57600).connect() 
    
    
data = {'nombre': ['yamparaez', 'toco', 'oruro', 'porco', 'cocapata'], 
        'provincia': [2012, 2012, 2013, 2014, 2014], 
        'departamento': ['chuquisaca', 'cochabamba', 'oruro', 'potosi', 'cochabamba']}
df = pd.DataFrame(data, index = [1,2,3,4,5])
fill_departamentos_var_t(con = database_connection)"""
