# %%
import pandas as pd
import numpy as np
import sqlalchemy
from sqlalchemy import create_engine
from sqlalchemy.exc import DBAPIError
import mysql.connector


def delete_duplicates(database_connection, tipo_de_voto, departamento_pk):

    query = """
    DELETE FROM public.votos
    WHERE voto_pk IN 
    (	
        SELECT voto_pk
        FROM 
        (
            SELECT voto_pk,
            ROW_NUMBER() OVER 
            ( 
                PARTITION BY partido_pk, mesa_pk, fecha_hora_actualizacion
                ORDER BY  voto_pk 
            ) AS row_num
            FROM public.votos 
            where departamento_pk = {0} and tipo_de_voto = '{1}'
        ) t
        WHERE t.row_num > 1 
    )
    """.format(departamento_pk, tipo_de_voto)
    #print("\n" + query + "\n")
    result = database_connection.execute(query)

    return result


if __name__ == "__main__":

    tipos_de_voto = ['gobernador', 'asambleista territorio',
                     'asambleista poblacion', 'alcalde', 'concejal']

    for departamento_pk in range(1, 10):
        print('Eliminando del departamento #{0}....'.format(departamento_pk))
        for voto in tipos_de_voto:
            print('Los votos de tipo: {0}'.format(voto))
            con = create_engine(
                "postgresql://electionData:electionDataPassword@34.122.17.115:5000/subnationals2021production", pool_recycle=3600, pool_timeout=57600)
            delete_duplicates(con, voto, departamento_pk)
            # con.close()
            print('Los votos fueron eliminados del departamento #{0} con el tipo de voto: {1}'.format(
                departamento_pk, voto))


# %%
