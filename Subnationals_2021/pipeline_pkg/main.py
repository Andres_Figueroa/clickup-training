import pandas as pd


def run_pipeline(folderpath):
    try:
        std_df_list = check_and_transform_to_standard_format(folderpath)
        fill_elecciones(std_df_list)
        fill_partido(std_df_list)
        fill_departamento(std_df_list)
        fill_municipio(std_df_list)
        fill_departamento_variante_en_tiempo(std_df_list)
        fill_municipio_variante_en_tiempo(std_df_list)
        fill_recinto(std_df_list)
        fill_mesa(std_df_list)
        fill_voto(std_df_list)
    except:
