# %%
import pandas as pd
import numpy as np
import sqlalchemy
import mysql.connector
import os
from pipeline_pkg.utils import EXCEL_ENGINE, con
from pipeline_pkg.upload import append_to_db_table
from pipeline_pkg.comparison_with_existing_records import compare_with_existing_records

# %%


def upload_to_db(upload_df, con, db_table_name='testTable'):
    print("Estableciendo Conexion")
    database_connection = con
    print("Insertando")

    success = append_to_db_table(df=upload_df,
                                 con=database_connection,
                                 table_name=db_table_name,
                                 schema='subnational2015',
                                 chunksize=1)

    #print('was it successful? ' + str(success))

    return success


def get_municipios_pk_from_db_munvt(database_connection=None):

    query = """SELECT municipio_pk, departamento_pk, nombre, provincia FROM public.municipio"""

    obtained_municipios_df = database_connection.execute(query)

    return_df = pd.DataFrame(
        {
            'municipio_pk': [],
            'departamento_pk': [],
            'nombre_municipio': [],
            'nombre_provincia': []
        },
    )

    id = 0
    for municipio in obtained_municipios_df:
        id += 1
        tmp_df = pd.DataFrame(
            {
                'municipio_pk': municipio[0],
                'departamento_pk': municipio[1],
                'nombre_municipio': municipio[2],
                'nombre_provincia': municipio[3]
            },
            index=[id],
        )
        return_df = return_df.append(tmp_df)
        # print(return_df)

    #return_df = 1
    return return_df


def get_elecciones_pk_from_db_munvt(database_connection, year):

    query = """SELECT elecciones_pk FROM public.elecciones 
                WHERE anio = {0}""".format(year)

    obtained_elecciones_df = database_connection.execute(query)
    print('llegue')
    return_df = pd.DataFrame(
        {
            'elecciones_pk': [],
        },
    )

    id = 0
    for elecciones in obtained_elecciones_df:
        id += 1
        tmp_df = pd.DataFrame(
            {
                'elecciones_pk': elecciones[0]
            },
            index=[id],
        )
        return_df = return_df.append(tmp_df)
        # print(return_df)

    #return_df = 1
    return return_df


def fill_municipios_var_t(year, inscritos_df=None, con=None, upload_table_name='testTable'):

    municipio_pks = get_municipios_pk_from_db_munvt(database_connection=con)

    elecciones_pk = get_elecciones_pk_from_db_munvt(
        database_connection=con, year=year)

    columna_elecciones_pk = np.full(
        shape=len(municipio_pks), fill_value=int(elecciones_pk.elecciones_pk[1]))

    municipio_pks.insert(1, 'elecciones_pk', columna_elecciones_pk)

    poblacion_municipios = pd.read_excel(os.path.join(
        os.getcwd(), 'poblacion_municipios.xlsx'), index_col=None, engine=EXCEL_ENGINE)

    upload_df = municipio_pks.join(poblacion_municipios.set_index(
        ['municipio', 'provincia']), on=['nombre_municipio', 'nombre_provincia'])
    # Fill null values with -1
    upload_df.loc[:, "poblacion"] = upload_df.loc[:, "poblacion"].fillna(-1)

    # Don't drop to check if there are errors
    # upload_df.dropna(subset = ['municipio_pk','departamento_pk'], inplace=True)

    upload_df = upload_df.drop(
        ['nombre_municipio', 'nombre_provincia', 'departamento'], axis=1)
    # se tiene que eliminar cuando se tengan los datos
    columna_poblacion_edad_voto = np.full(shape=len(upload_df), fill_value=0)
    upload_df.insert(4, 'poblacion_edad_voto', columna_poblacion_edad_voto)
    upload_df.loc[:, "poblacion_edad_voto"] = upload_df.loc[:,
                                                            "poblacion_edad_voto"].fillna(-1)

    clean_df = compare_with_existing_records(upload_df, con, 'municipio_variante_en_tiempo', [
                                             'municipio_pk', 'elecciones_pk', 'departamento_pk'])
    if clean_df is not None and not clean_df.empty:
        upload_df = clean_df.join(upload_df.set_index(['municipio_pk', 'elecciones_pk', 'departamento_pk']), on=[
                                  'municipio_pk', 'elecciones_pk', 'departamento_pk'])

        upload_df = upload_df[['municipio_pk', 'elecciones_pk',
                               'departamento_pk', 'poblacion_edad_voto', 'poblacion']]
        return upload_to_db(upload_df=upload_df, con=con, db_table_name=upload_table_name)
    else:
        print("No new municipios variantes en el tiempo...")

# %%
# Update poblacion


def update_poblacion_mun_var_t(filepath, year, con):
    TABLE_NAME = "municipio_variante_en_tiempo"
    TEMP_TABLE_NAME = "temp_mvt"
    schema = "public"
    poblacion_municipios = pd.read_excel(os.path.join(
        os.getcwd(), filepath), index_col=None, engine=EXCEL_ENGINE)
    mun_var_t_with_names_df = pd.read_sql_query(
        f"SELECT t.elecciones_pk,t.municipio_pk,t.departamento_pk,t.municipio,t.provincia,t.departamento FROM (SELECT mun.municipio_pk, mun.nombre as municipio, dep.nombre as departamento, mun.provincia, elec.elecciones_pk, mun.departamento_pk FROM {TABLE_NAME} mvt INNER JOIN municipio mun ON (mvt.municipio_pk=mun.municipio_pk AND mvt.departamento_pk=mun.departamento_pk) INNER JOIN elecciones elec ON elec.anio = {year} INNER JOIN departamento dep ON dep.departamento_pk = mvt.departamento_pk ) as  t", con=con).drop_duplicates()
    upload_df = mun_var_t_with_names_df.join(poblacion_municipios.set_index(['municipio', 'provincia', 'departamento']), on=[
                                             'municipio', 'provincia', 'departamento']).drop(columns=['municipio', 'provincia', 'departamento'])
    upload_df.loc[:, "poblacion"] = upload_df.loc[:, "poblacion"].fillna(-1)
    append_to_db_table(upload_df, con, TEMP_TABLE_NAME,
                       schema, if_exists='replace')
    sql = f"UPDATE {TABLE_NAME} AS mvt" + \
        " SET poblacion = t.poblacion" + \
        " FROM temp_mvt AS t" + \
        " WHERE (mvt.municipio_pk = t.municipio_pk AND mvt.elecciones_pk = t.elecciones_pk AND mvt.departamento_pk = t.departamento_pk)"
    with con.begin() as conn:     # TRANSACTION
        conn.execute(sql)


def check_records_with_value(table_name, columns, target_column, value, con, year):
    df = pd.read_sql_table(table_name, con, columns=columns)
    elec_pk = get_elecciones_pk(year, con)
    print(df[df[target_column] == value & df["elecciones_pk"] == elec_pk])


# %%
# #test cell
# TABLE_NAME = "municipio_variante_en_tiempo"
# TEMP_TABLE_NAME = "temp_mvt"
# schema = "public"
# year = 2021
# poblacion_municipios = pd.read_excel(os.path.join(os.getcwd(),'poblacion_municipios.xlsx'), index_col=None,engine=EXCEL_ENGINE)
# mun_var_t_with_names_df = pd.read_sql_query(f"SELECT t.elecciones_pk,t.municipio_pk,t.departamento_pk,t.municipio,t.provincia,t.departamento FROM (SELECT mun.municipio_pk, mun.nombre as municipio, dep.nombre as departamento, mun.provincia, elec.elecciones_pk, mun.departamento_pk FROM {TABLE_NAME} mvt INNER JOIN municipio mun ON (mvt.municipio_pk=mun.municipio_pk AND mvt.departamento_pk=mun.departamento_pk) INNER JOIN elecciones elec ON elec.anio = {year} INNER JOIN departamento dep ON dep.departamento_pk = mvt.departamento_pk ) as  t",con=con).drop_duplicates()
# upload_df = mun_var_t_with_names_df.join(poblacion_municipios.set_index(['municipio','provincia','departamento']), on=['municipio','provincia','departamento']).drop(columns=['municipio','provincia','departamento'])
# upload_df.loc[:,"poblacion"] = upload_df.loc[:,"poblacion"].fillna(-1)
# # %%
# append_to_db_table(upload_df, con, TEMP_TABLE_NAME, schema,if_exists='replace')
# # %%
# sql = f"UPDATE {TABLE_NAME} AS mvt" + \
#       " SET poblacion = t.poblacion" + \
#       " FROM temp_mvt AS t" + \
#       " WHERE (mvt.municipio_pk = t.municipio_pk AND mvt.elecciones_pk = t.elecciones_pk AND mvt.departamento_pk = t.departamento_pk)"
# # %%
# with con.begin() as conn:     # TRANSACTION
#     conn.execute(sql)
# %%
"""
database_username = 'electionData'
database_password = 'electionDataPassword'
database_ip       = '34.122.17.115:5000'
database_name     = 'electionData2020'
database_connection = sqlalchemy.create_engine('postgresql://{0}:{1}@{2}/{3}'.
                                            format(database_username,
                                                    database_password, 
                                                    database_ip,
                                                    database_name),
                                            pool_recycle=1,
                                            pool_timeout=57600).connect() 
    
    
data = {'nombre': ['yamparaez', 'toco', 'oruro', 'porco', 'cocapata'], 
        'provincia': [2012, 2012, 2013, 2014, 2014], 
        'departamento': ['chuquisaca', 'cochabamba', 'oruro', 'potosi', 'cochabamba']}
df = pd.DataFrame(data, index = [1,2,3,4,5])
fill_departamentos_var_t(con = database_connection)"""
# %%
