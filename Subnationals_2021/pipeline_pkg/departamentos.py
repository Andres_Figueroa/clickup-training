# %%
import pandas as pd
import numpy as np
import sqlalchemy
import mysql.connector
import os
from pipeline_pkg.standard_form_transformer import transform_to_standard_format
from pipeline_pkg.cleaning import clean_standard_form_df
from algorithms.utils import *
from pipeline_pkg.upload import append_to_db_table
from pipeline_pkg.comparison_with_existing_records import compare_with_existing_records

# %%


def upload_to_db(upload_df, con=None, db_table_name='testTable'):
    print("Estableciendo Conexion")
    database_connection = con

    print("Insertando")

    success = append_to_db_table(df=upload_df,
                                 con=database_connection,
                                 table_name=db_table_name,
                                 schema='public',
                                 chunksize=1)

    #print('was it successful? ' + str(success))

    return success


def fill_departamentos(departamentos_df, db_table_name, con=None):

    departamentos_df = compare_with_existing_records(
        df=departamentos_df, con=con, table_name=db_table_name, col_ids=["nombre_departamento"])

    if departamentos_df is not None and not departamentos_df.empty:
        departamentos_filtered = departamentos_df[[
            'nombre_departamento']].drop_duplicates()
        print(departamentos_filtered)
        departamentos_filtered = departamentos_filtered.rename(
            columns={'nombre_departamento': 'nombre'})

        departamentos_filtered.loc[:, "pais"] = "bolivia"

        # bolivias = np.full(shape = len(departamentos_filtered), fill_value = 'bolivia')
        # departamentos_filtered.insert(1,'pais', bolivias)

        return upload_to_db(upload_df=departamentos_filtered, con=con, db_table_name=db_table_name)
    else:
        print('No new departamentos...')


# %%
if __name__ == '__main__':
    folderpath = "../../../dataset_2015"
    filenames = os.listdir(folderpath)
    test_filepath = os.path.join(folderpath, filenames[8])
    test_df = pd.read_excel(test_filepath, engine=EXCEL_ENGINE)
    std_test_df = transform_to_standard_format(test_filepath, 2015)
    clean_std_test_df = clean_standard_form_df(std_test_df)
    # Fails when using subset due to assert on size
    fill_departamentos(clean_std_test_df, con, 2015)
# %%
