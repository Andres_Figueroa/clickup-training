import pandas as pd
import unidecode as un

# ---------------------------------------------------------------------------- #
#                              General guidelines
# - Add as many functions as necessary
# - Change the function definition if convenient and necessary (also the comments)
# - Try to make the code as reusable as possible (general functions)
# - Try to do the code as clean as posible
# - Dataframe variables end with "_df" (E.g. votes_df)
# - Comment the code in the way that is shown here
# - Follow the style shown in the template
# - Test the main function with a small dataset
# (E.g. variables in snake case (E.g. votes_per_day), clases CamelCase if any)
# ---------------------------------------------------------------------------- #


def clean_standard_form_df(std_df):
    """Cleans a dataframe in the standard form

    The standard form was defined in a way to convey all the info in a simple strcuture (ask someone how is the standard form)

    Args:
        std_df (:obj:pd.DataFrame): A dataframe in the standard form
    Returns:
        a dataframe with the following characteristics:
            - Colnames: snake lower case singular (nombre_municipio). Keep a convention
            - String cols:
                - keep the ñ's
                - remove all special characters (*,&,! etc)
                - remove all the head and trailing spaces
                - replace nulls with a "null" string or a convention if specified
                - from data exploration, replace equivalent names (challapa, chayapa are the same, pick challapa and replace all chayapa by challapa)
            - Float cols:
                - Asign the corresponding datatype in the dataframe
                - np.nan replaced by 0 or any convention specified
            - Int cols:
                - Asign the correspoding datatype in the dataframe
                - np.nan replaced by 0 or any convention specified

    """
    def lower(x): return x.lower()
    def repn(x): return x.replace('ñ', '[')
    def devn(x): return x.replace('[', 'ñ')
    def repcom(x): return x.replace('\"', '\'')
    def spa(x): return x.strip()
    std_df.loc[:, std_df.dtypes == 'object'] = std_df.loc[:,
                                                          std_df.dtypes == 'object'].fillna("")
    std_df.loc[:, std_df.dtypes == 'object'] = std_df.loc[:,
                                                          std_df.dtypes == 'object'].applymap(lower)
    std_df.loc[:, std_df.dtypes == 'object'] = std_df.loc[:,
                                                          std_df.dtypes == 'object'].applymap(repn)
    std_df.loc[:, std_df.dtypes == 'object'] = std_df.loc[:,
                                                          std_df.dtypes == 'object'].applymap(un.unidecode)
    std_df.loc[:, std_df.dtypes == 'object'] = std_df.loc[:,
                                                          std_df.dtypes == 'object'].applymap(repcom)
    std_df.loc[:, std_df.dtypes == 'object'] = std_df.loc[:,
                                                          std_df.dtypes == 'object'].applymap(devn)
    std_df.loc[:, std_df.dtypes == 'object'] = std_df.loc[:,
                                                          std_df.dtypes == 'object'].applymap(spa)

    return std_df


# Add more functions here if required
