# %%
import pandas as pd
import numpy as np
import openpyxl
import os
from pipeline_pkg.comparison_with_existing_records import *
from pipeline_pkg.standard_form_transformer import transform_to_standard_format
from pipeline_pkg.utils import *
from pipeline_pkg.cleaning import clean_standard_form_df
from pipeline_pkg.upload import *

EXCEL_ENGINE = "openpyxl"
# %%


def fill_mesa(std_df, con, year):
    COLS_STD_FORMAT = [
        "mesa_pk",
        "nombre_municipio",
        "nombre_recinto",
        "nombre_departamento",
        "numero",
        "inscritos",
    ]
    COL_IDS = ["mesa_pk"]
    TABLE_NAME = "mesa"
    SCHEMA = "public"

    print(
        "//////////////////////////// FILL MESA ////////////////////////////"
    )
    print("Comparing mesa inputed records with existing records..")
    new_records_df = compare_with_existing_records(
        std_df, con, TABLE_NAME, COL_IDS
    )
    if (
        new_records_df is not None
        and not new_records_df.empty
        and new_records_df.shape[0] != 0
    ):
        new_std_records_df = std_df.merge(
            new_records_df, on=COL_IDS, how="inner"
        )
        print(f"{new_records_df.shape[0]} new mesa records returned")
        print(f"Select mesa relevant cols: {COLS_STD_FORMAT}")
        print("Start cleaning process...")
        new_rel_df = new_std_records_df.loc[
            :, COLS_STD_FORMAT
        ].drop_duplicates()
        print(f"Before adding pks: {new_rel_df.columns}")
        new_clean_rel_pks_records_df = replace_names_with_pks(
            new_rel_df, "recinto", con
        )
        new_clean_rel_pks_records_df.loc[
            :, "elecciones_pk"
        ] = get_elecciones_pk(year, con)
        print("After adding pks: ", new_clean_rel_pks_records_df.columns)
        print(
            f"Load {new_clean_rel_pks_records_df.shape[0]} records to mesa..."
        )
        append_to_db_table(
            new_clean_rel_pks_records_df, con, TABLE_NAME, SCHEMA
        )
        print("Load completed")
        return new_clean_rel_pks_records_df
    else:
        print("No new mesas...")
    print(
        "//////////////////////////// END FILL MESA ////////////////////////////"
    )


# %%
if __name__ == "__main__":
    folderpath = "../../../dataset_2015"
    filenames = os.listdir(folderpath)
    test_filepath = os.path.join(folderpath, filenames[0])
    test_df = pd.read_excel(test_filepath, engine=EXCEL_ENGINE)
    std_test_df = transform_to_standard_format(test_filepath, 2015)
    clean_std_test_df = clean_standard_form_df(std_test_df)
    fill_mesa(clean_std_test_df, con, 2015)


# %%
