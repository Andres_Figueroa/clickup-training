import pandas as pd
import numpy as np
import sqlalchemy
import mysql.connector

from pipeline_pkg.upload import append_to_db_table
from pipeline_pkg.comparison_with_existing_records import compare_with_existing_records

# %%


def upload_to_db(upload_df, con, db_table_name='testTable'):
    print("Estableciendo Conexion")
    database_connection = con
    print("Insertando")

    success = append_to_db_table(df=upload_df,
                                 con=database_connection,
                                 table_name=db_table_name,
                                 schema='subnational2015',
                                 chunksize=1)

    #print('was it successful? ' + str(success))

    return success


def get_columnas_db_mun():
    return ['poblacion_2020',
            'tasa_pobreza_energetica_extrema_2016',
            'porc_necesidades_basicas_insatisfechas_2012',
            'porc_poblacion_educacion_superior_mayor_19_2012',
            'capacidad_ejecucion_presupuesto_programado_2017',
            'indice_de_pobreza_multidimensional_2012',
            'perc_acceso_servicios_basicos_2012',
            'bolivia_index_score',
            'tasa_abandono_secundaria_hombres_2017',
            'inversion_publica_per_capita_bs',
            'tasa_homicidios_registrados_prom_2015_2017_100k_hab',
            'coeficiente_gini_consumo_electrico_2016',
            'poblacion_no_habla_espaniol_mayor_3_2012',
            'tasa_abandono_secundaria_mujeres_2017',
            'tasa_de_alfabetizacion_mayor_15_2012',
            'consumo_electricidad_residencial_per_capita_2016',
            'proporcion_ingresos_municipales_impuestos_locales_2017',
            'porc_hombres_no_estudian_no_trabajan_15_24_2012',
            'porc_mujeres_no_estudian_no_trabajaon_15_24_2012',
            ]


def get_municipios_from_db_mun():
    database_username = 'electionData'
    database_password = 'electionDataPassword'
    database_ip = '34.122.17.115:5000'
    database_name = 'electionData2020'
    database_connection = sqlalchemy.create_engine('postgresql://{0}:{1}@{2}/{3}'.
                                                   format(database_username,
                                                          database_password,
                                                          database_ip,
                                                          database_name),
                                                   pool_recycle=1,
                                                   pool_timeout=57600).connect()

    query = """SELECT nombre,provincia, es_rural FROM public.municipio
                ORDER BY departamento_pk """

    obtained_municipios_df = database_connection.execute(query)

    return_df = pd.DataFrame(
        {
            'nombre_municipio': [],
            'nombre_provincia': [],
            'es_rural': []
        },
    )

    id = 0
    for municipio in obtained_municipios_df:
        id += 1
        tmp_df = pd.DataFrame(
            {
                'nombre_municipio': municipio[0],
                'nombre_provincia': municipio[1],
                'es_rural': municipio[2]
            },
            index=[id],
        )
        return_df = return_df.append(tmp_df)
        # print(return_df)
    database_connection.close()
    return return_df


def get_dapartamentos_from_db_mun(con):
    database_connection = con

    query = """SELECT * FROM public.departamento
                WHERE pais like 'bolivia'"""

    obtained_departamentos_df = database_connection.execute(query)

    return_df = pd.DataFrame(
        {
            'departamento_pk': [],
            'nombre': []
        },
    )

    # print('llegue')
    id = 0
    for departamento in obtained_departamentos_df:
        id += 1
        tmp_df = pd.DataFrame(
            {
                'departamento_pk': departamento[0],
                'nombre': departamento[1]
            },
            index=[id],
        )
        return_df = return_df.append(tmp_df)
        # print(return_df)

    return return_df


def fill_municipios(municipios_df, db_table_name, con=None):

    municipios_df = compare_with_existing_records(df=municipios_df, con=con, table_name=db_table_name, col_ids=[
                                                  'nombre_municipio', 'nombre_provincia', 'nombre_departamento'])

    if municipios_df is not None and not municipios_df.empty:
        municipios_filtered = municipios_df[['nombre_municipio', 'nombre_provincia', 'nombre_departamento']].drop_duplicates(
            subset=['nombre_municipio', 'nombre_provincia', 'nombre_departamento'])

        municipios_db_table = get_municipios_from_db_mun()

        municipios_filtered = municipios_filtered.join(municipios_db_table.set_index(
            ['nombre_municipio', 'nombre_provincia']), on=['nombre_municipio', 'nombre_provincia'])
        municipios_filtered[['es_rural']] = municipios_filtered[[
            'es_rural']].astype('bool')

        departamentos_db_table = get_dapartamentos_from_db_mun(con=con)

        municipios_filtered = municipios_filtered.join(
            departamentos_db_table.set_index('nombre'), on='nombre_departamento')
        municipios_filtered.dropna(subset=["departamento_pk"], inplace=True)
        municipios_filtered[['departamento_pk']] = municipios_filtered[[
            'departamento_pk']].astype('int')

        ceros = np.full(shape=len(municipios_filtered), fill_value=0)
        nombres_columnas = get_columnas_db_mun()
        for i in range(2, 21):
            municipios_filtered.insert(i, nombres_columnas[i-2], ceros)

        upload_df = municipios_filtered.drop(['nombre_departamento'], axis=1)

        upload_df = upload_df.rename(
            columns={'nombre_municipio': 'nombre', 'nombre_provincia': 'provincia'})
        return upload_to_db(upload_df=upload_df, con=con, db_table_name=db_table_name)
    else:
        print("No new municipios...")
    # print(upload_df)
    # print(upload_df.dtypes)


# %%
"""    
data = {'nombre': ['yamparaez', 'toco', 'oruro', 'porco', 'cocapata'], 
        'provincia': [2012, 2012, 2013, 2014, 2014], 
        'departamento': ['chuquisaca', 'cochabamba', 'oruro', 'potosi', 'cochabamba']}
df = pd.DataFrame(data, index = [1,2,3,4,5])
fill_municipios(municipios_df = df)"""
