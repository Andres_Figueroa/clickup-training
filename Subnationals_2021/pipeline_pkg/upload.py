import pandas as pd
import numpy as np
import sqlalchemy
from sqlalchemy.exc import DBAPIError
import mysql.connector

# ---------------------------------------------------------------------------- #
#                              General guidelines
# - Add as many functions as necessary
# - Change the function definition if convenient and necessary (also the comments)
# - Try to make the code as reusable as possible (general functions)
# - Try to do the code as clean as posible
# - Dataframe variables end with "_df" (E.g. votes_df)
# - Comment the code in the way that is shown here
# - Follow the style shown in the template
# - Test the main function with a small dataset
# (E.g. variables in snake case (E.g. votes_per_day), clases CamelCase if any)
# ---------------------------------------------------------------------------- #

# %%


def append_to_db_table(
    df,
    con,
    table_name,
    schema,
    if_exists="append",
    method="multi",
    use_index=False,
    index_label=None,
    chunksize=None,
):
    """Appends a df to a database table

    After the tables are all cleaned and transformed to match the database tables, they are ready to upload
    Prints messages of success
    Raises error messages when it is not possible to do the append

    Args:
        df (:obj:`pd.DataFrame`): Dataframe to be appended to the databse table
        con (:obj:`sqlalchemy.engine.Engine`): Database connection engine
        table_name (str): Database table name
        schema (str): schema name in database
        useIndex (bool): True to import dataframe index as a column / default False
        indexLabel (str): Name for the index table column / default None
        chunksize (int): Number of rows in a batch to be writen at a time / default None (All at once)
    """
    successful_transaction = False
    retry_count = 0

    while (not successful_transaction) and (retry_count < 5):
        try:
            print(
                "Appending to: \n DB: "
                + schema
                + "\n Table: "
                + table_name
                + "\n"
            )
            df.to_sql(
                name=table_name,
                con=con,
                if_exists=if_exists,
                index=use_index,
                index_label=index_label,
                method=method,
                chunksize=chunksize,
            )
            successful_transaction = True

        except DBAPIError as e:
            print(e)
            retry_count += 1
            successful_transaction = False

    return successful_transaction


# %%
"""
Test Code Just in Case is Needed
         
print("Conexion")    
database_username = 'electionData'
database_password = 'electionDataPassword'
database_ip       = '34.122.17.115:5000'
database_name     = 'electionData2020'
database_connection = sqlalchemy.create_engine('postgresql://{0}:{1}@{2}/{3}'.
                                               format(database_username,
                                                      database_password, 
                                                      database_ip,
                                                      database_name),
                                               pool_recycle=1,
                                               pool_timeout=57600).connect()    
print("Insertando")    
inserted_df = pd.DataFrame(np.array([['Nombre 4AS', 'Pais 4AS'],
                                     ['Nombre 5AS', 'Pais 5AS'],
                                     ['Nombre 6AS', 'Pais 6AS']]),
                           columns = ['nombre','pais']) 


success = append_to_db_table(inserted_df, database_connection, 'testTable', 'electionData2020')
print('was it successful? ' + str(success))"""
# Add more functions here if required
