import pandas as pd
import unidecode as un

# ---------------------------------------------------------------------------- #
#                              General guidelines
# - Add as many functions as necessary
# - Change the function definition if convenient and necessary (also the comments)
# - Try to make the code as reusable as possible (general functions)
# - Try to do the code as clean as posible
# - Dataframe variables end with "_df" (E.g. votes_df)
# - Comment the code in the way that is shown here
# - Follow the style shown in the template
# - Test the main function with a small dataset
# (E.g. variables in snake case (E.g. votes_per_day), clases CamelCase if any)
# ---------------------------------------------------------------------------- #


def clean_standard_form_df(std_df):
    """Cleans a dataframe in the standard form
    
    The standard form was defined in a way to convey all the info in a simple strcuture (ask someone how is the standard form)
    
    Args:
        std_df (:obj:pd.DataFrame): A dataframe in the standard form
    Returns:
        a dataframe with the following characteristics:
            - Colnames: snake lower case singular (nombre_municipio). Keep a convention
            - String cols:
                - keep the ñ's
                - remove all special characters (*,&,! etc)
                - remove all the head and trailing spaces
                - replace nulls with a "null" string or a convention if specified
                - from data exploration, replace equivalent names (challapa, chayapa are the same, pick challapa and replace all chayapa by challapa)
            - Float cols:
                - Asign the corresponding datatype in the dataframe
                - np.nan replaced by 0 or any convention specified
            - Int cols:
                - Asign the correspoding datatype in the dataframe
                - np.nan replaced by 0 or any convention specified
    
    """
    lower = lambda x: x.lower()
    repn = lambda x: x.replace("ñ", "[")
    repn = lambda x: x.replace("Ñ", "[")
    devn = lambda x: x.replace("[", "ñ")
    repcom = lambda x: x.replace('"', "'")
    fill_null = lambda x: str(x) if pd.notnull(x) else "-1"
    std_df.loc[:, std_df.dtypes == "object"] = std_df.loc[
        :, std_df.dtypes == "object"
    ].applymap(fill_null)
    std_df.loc[:, std_df.dtypes == "object"] = std_df.loc[
        :, std_df.dtypes == "object"
    ].applymap(repn)
    std_df.loc[:, std_df.dtypes == "object"] = std_df.loc[
        :, std_df.dtypes == "object"
    ].applymap(un.unidecode)
    std_df.loc[:, std_df.dtypes == "object"] = std_df.loc[
        :, std_df.dtypes == "object"
    ].applymap(lower)
    std_df.loc[:, std_df.dtypes == "object"] = std_df.loc[
        :, std_df.dtypes == "object"
    ].applymap(repcom)
    std_df.loc[:, std_df.dtypes == "object"] = std_df.loc[
        :, std_df.dtypes == "object"
    ].applymap(devn)

    s = string_row(std_df)
    std_df = std_df[std_df[s] != "-1"]

    return std_df


# Add more functions here if required
def string_row(data):
    s = data.dtypes == "object"
    for index, value in s.items():
        if value == True:
            return index
