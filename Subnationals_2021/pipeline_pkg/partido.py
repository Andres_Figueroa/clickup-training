import pandas as pd
import numpy as np
import sqlalchemy
import mysql.connector

from pipeline_pkg.upload import append_to_db_table
from pipeline_pkg.comparison_with_existing_records import compare_with_existing_records

# %%


def get_elecciones_pk_from_db_partido(database_connection, year):

    query = """SELECT elecciones_pk FROM public.elecciones 
                WHERE anio = {0}""".format(year)

    obtained_elecciones_df = database_connection.execute(query)
    print('llegue')
    return_df = pd.DataFrame(
        {
            'elecciones_pk': [],
        },
    )

    id = 0
    for elecciones in obtained_elecciones_df:
        id += 1
        tmp_df = pd.DataFrame(
            {
                'elecciones_pk': elecciones[0]
            },
            index=[id],
        )
        return_df = return_df.append(tmp_df)
        # print(return_df)

    return return_df


def fill_partido(partidos_df, schema, db_table_name='partido', con=None, year=0, elections_pk=-1,  tendencia=None, valor_tendencia=None, cod_tendencia=None, ref_partido=None):

    partidos_df = compare_with_existing_records(
        partidos_df, con,  db_table_name, ['partido'])
    if partidos_df is not None and not partidos_df.empty:
        nombres_partidos = partidos_df.partido.unique()

        cantidad_partidos = len(nombres_partidos)
        elections_pk = get_elecciones_pk_from_db_partido(con, year)
        columna_elec_pk = np.full(
            shape=cantidad_partidos, fill_value=int(elections_pk.elecciones_pk[1]))

        if tendencia is None:
            columna_tendencia = np.full(
                shape=cantidad_partidos, fill_value=" ")
        elif valor_tendencia is not None:
            columna_tendencia = tendencia

        if valor_tendencia is None:
            columna_vtendencia = np.full(shape=cantidad_partidos, fill_value=0)
        elif valor_tendencia is not None:
            columna_vtendencia = valor_tendencia

        if cod_tendencia is None:
            columna_ctendencia = np.full(
                shape=cantidad_partidos, fill_value=-1)
        elif cod_tendencia is not None:
            columna_ctendencia = cod_tendencia

        columna_ref_partido = np.full(shape=cantidad_partidos, fill_value=None)
        for i in range(cantidad_partidos):
            columna_ref_partido[i] = str(year) + "_" + nombres_partidos[i]

        upload_df = pd.DataFrame(
            {
                'elecciones_pk': columna_elec_pk,
                'nombre': nombres_partidos,
                # 'tendencia' : columna_tendencia,
                'valor_tendencia': columna_vtendencia,
                'cod_tendencia': columna_ctendencia,
                'ref_partido': columna_ref_partido
            },
            index=range(cantidad_partidos),
        )
        print("Estableciendo Conexion")

        database_connection = con
        print("Insertando")

    # print(upload_df)

        success = append_to_db_table(df=upload_df,
                                     con=database_connection,
                                     table_name=db_table_name,
                                     schema=schema,
                                     chunksize=1)

        print('was it successful? ' + str(success))

        return success
    else:
        print("No new partidos...")


# %%

"""
data = {'partidos': ['Jason', 'Amy', 'Tina', 'Jake', 'Amy'], 
        'year': [2012, 2012, 2013, 2014, 2014], 
        'reports': [4, 24, 31, 2, 3]}
df = pd.DataFrame(data, index = ['Cochice', 'Pima', 'Santa Cruz', 'Maricopa', 'Yuma'])
fill_partido(df, 2021)
"""
# %%
