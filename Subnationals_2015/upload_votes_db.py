# %%
from sqlalchemy import create_engine
from datetime import datetime
import pandas as pd
import os
import openpyxl
# %%
# Set the excel engine
EXCEL_ENGINE='openpyxl'
dataset_folder = "Cleaned_datasets_2015/"
filename = "VOTOS CBBA.xlsx"
# Read the file
file_path = os.path.join(dataset_folder,filename)
votos_cbba_df = pd.read_excel(file_path,engine=EXCEL_ENGINE)
votos_cbba_df.head()
# %%
# Exploring data
votos_cbba_df.dtypes
# %%
# Correct table values
votos_cbba_df= votos_cbba_df.rename({"num_vuelta":"vuelta"},axis=1)
votos_cbba_df.rename({"ruta_del_achivo_base":"ruta_del_archivo_base"},axis=1)
votos_cbba_df= votos_cbba_df.rename({"estado_acta":"estado_de_acta"},axis=1)
votos_cbba_df.loc[:,"estado_de_acta"] = votos_cbba_df.loc[:,"estado_de_acta"].replace({"computado":"Completado"})
votos_cbba_df.loc[:,"fecha_hora_actualizacion"] = datetime.today()
votos_cbba_df.loc[:,"tipo_de_computo"] =bytes(1)
# %%
# drop index column
votos_cbba_df.columns
# %%
# Creating connection to database
con = create_engine("postgresql://electionData:electionDataPassword@34.122.17.115:5000/subnational2021")
votos_cbba_df.to_sql("votos",con= con,if_exists='append',method='multi',index=False)

# %%
