#%%
from benfordslaw import benfordslaw
import pandas as pd
import sqlalchemy as db

engine = db.create_engine("postgres://electionData:electionDataPassword@34.122.17.115:5000/subnationals2021production")
con = engine.connect()


query = """
select concat(recinto.nombre,',',municipio.nombre) as recinto, concat(departamento.nombre,'.',partido.nombre) as partido, votos from
(select recinto_pk, municipio_pk,departamento_pk, partido_pk, sum(cantidad) as votos from votos
where tipo_de_voto = 'gobernador'
and fecha_hora_actualizacion in (select max(fecha_hora_actualizacion) as fecha_hora_actualizacion from votos group by departamento_pk)
and partido_pk != 8 and partido_pk != 9 and departamento_pk = 7
group by recinto_pk, municipio_pk,departamento_pk,partido_pk
order by municipio_pk,departamento_pk) as t1
inner join recinto on t1.recinto_pk = recinto.recinto_pk
inner join municipio on t1.municipio_pk = municipio.municipio_pk
inner join partido on t1.partido_pk = partido.partido_pk 
inner join departamento on t1.departamento_pk = departamento.departamento_pk 
"""


df = pd.read_sql_query(query,con)

p_df = df.pivot(index='recinto', columns='partido')

p_df = p_df['votos'].reset_index()
p_df.columns.name = None

p_df = p_df.fillna(0)
#%%
#PLOT TODOS LOS PARTIDOS
for par in p_df.columns[1:]:
    print(par)
    bl = benfordslaw(alpha=0.05,pos=1)
    X = p_df[str(par)].values
    results = bl.fit(X) 
    bl.plot(title=str(par)) 
    

# %%
